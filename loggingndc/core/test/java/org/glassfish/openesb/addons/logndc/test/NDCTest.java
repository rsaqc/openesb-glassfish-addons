/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.logndc.test;

import com.sun.appserv.server.LifecycleEvent;
import org.glassfish.openesb.addons.logndc.NDCLifeCycleListener;
import org.glassfish.openesb.addons.logndc.NDCProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import junit.framework.TestCase;

public class NDCTest extends TestCase {
    private Handler[] mExistingHandlers;
    
    public void setUp() {
        mExistingHandlers = Logger.getLogger("").getHandlers();
    }
    
    public void tearDown() {
        Handler[] now = Logger.getLogger("").getHandlers();
        for (int i = 0; i < now.length; i++) {
            Logger.getLogger("").removeHandler(now[i]);
        }
        for (Handler handler : mExistingHandlers) {
            Logger.getLogger("").addHandler(handler);
        }
    }
    
    public void testNormal() throws Throwable {
        // Create
        NDCLifeCycleListener life = new NDCLifeCycleListener();
        life.handleEvent(new LifecycleEvent(this, LifecycleEvent.INIT_EVENT, null, null));

        // Setup
        final ArrayList<LogRecord> records = new ArrayList<LogRecord>();
        Handler h = new Handler() {
            @Override
            public void close() throws SecurityException {
            }

            @Override
            public void flush() {
            }

            @Override
            public void publish(LogRecord record) {
                records.add(record);
            }
        };
        Logger.getLogger("").addHandler(h);
        
        Logger.getLogger("x").info("msg1");  // should log
        Logger.getLogger("x").fine("msg2");  // should not log
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters() == null);
        records.clear();
        
        // push c1
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        // push c2
        Logger.getLogger(NDCProvider.ENTERCONTEXT).info("c2");
        
        // c1,c2 should not have been logged
        assertTrue(records.size() == 0);
        
        // Log a msg
        Logger.getLogger("x").info("msg1");  // should log
        assertTrue(records.size() == 1);
        
        // Check context
        assertTrue(records.get(0).getParameters().length == 1);
        Map m = (Map) records.get(0).getParameters()[0];
        assertTrue(m.get(NDCProvider.CONTEXT).toString().equals("c2"));
        
        // pop c2
        records.clear();
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("c2");
        Logger.getLogger("x").info("msg1");  // should log
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 1);
        m = (Map) records.get(0).getParameters()[0];
        assertTrue(m.get(NDCProvider.CONTEXT).toString().equals("c1"));
        
        // pop c1
        records.clear();
        Logger.getLogger(NDCProvider.EXITCONTEXT).config("c1");
        Logger.getLogger("x").info("msg1");  // should log
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters() == null);
    }

    public void testStackCorruption() throws Throwable {
        // Create
        NDCLifeCycleListener life = new NDCLifeCycleListener();
        life.handleEvent(new LifecycleEvent(this, LifecycleEvent.INIT_EVENT, null, null));
        
        // Setup
        final ArrayList<LogRecord> records = new ArrayList<LogRecord>();
        Handler h = new Handler() {
            @Override
            public void close() throws SecurityException {
            }
            @Override
            public void flush() {
            }
            @Override
            public void publish(LogRecord record) {
                records.add(record);
            }
        };
        Logger.getLogger("").addHandler(h);

        // push c1
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        assertTrue(records.size() == 0);

        // pop c2: incorrect
        Logger.getAnonymousLogger().info("Popping non existent context");
        records.clear();
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("not c1");
        assertTrue(records.size() == 1);
        records.clear();
        
        // pop c1: correct
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("c1");
        assertTrue(records.size() == 0);
        
        // push c1, c2
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c2");
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c3");
        assertTrue(records.size() == 0);
        
        // pop c1, forget about c2, c3
        Logger.getAnonymousLogger().info("Popping existent context, skipping one");
        records.clear();
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("c1");
        assertTrue(records.size() == 1);
        records.clear();

        // pop c2: incorrect
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("c2");
        assertTrue(records.size() == 1);
        records.clear();
        
        // stack underflow
        Logger.getLogger(NDCProvider.EXITCONTEXT).info("c2");
        assertTrue(records.size() == 1);
        records.clear();
        
        // stack overflow
        for (int i = 0; i < 101; i++) {
            Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        }
        assertTrue(records.size() == 1);
        records.clear();

        for (int i = 0; i < 102; i++) {
            Logger.getLogger(NDCProvider.EXITCONTEXT).fine("c1");
        }
        assertTrue(records.size() == 1);
        records.clear();
    }

    public void testTypes() throws Throwable {
        // Create
        NDCLifeCycleListener life = new NDCLifeCycleListener();
        life.handleEvent(new LifecycleEvent(this, LifecycleEvent.INIT_EVENT, null, null));
        
        // Setup
        final ArrayList<LogRecord> records = new ArrayList<LogRecord>();
        Handler h = new Handler() {
            @Override
            public void close() throws SecurityException {
            }
            @Override
            public void flush() {
            }
            @Override
            public void publish(LogRecord record) {
                records.add(record);
            }
        };
        Logger.getLogger("").addHandler(h);
        
        // context: none; log: none
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().info("msg");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters() == null);
        records.clear();

        // context: none; log: Object
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}", "1");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 1);
        assertTrue(records.get(0).getParameters()[0] instanceof String);
        assertTrue(records.get(0).getParameters()[0].toString().equals("1"));
        records.clear();

        // context: String; log: none
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().info("msg");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 1);
        assertTrue(records.get(0).getParameters()[0] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[0]).get(NDCProvider.CONTEXT).toString().equals("c1"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).fine("c1");
        records.clear();
        
        // context: String; log: Object
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}", "1");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 2);
        assertTrue(records.get(0).getParameters()[1] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[1]).get(NDCProvider.CONTEXT).toString().equals("c1"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).fine("c1");
        records.clear();
        
        // context: String; log: Object[]
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2"});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 3);
        assertTrue(records.get(0).getParameters()[2] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[2]).get(NDCProvider.CONTEXT).toString().equals("c1"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).fine("c1");
        records.clear();
        
        // context: String; log: Object[], Map
        Logger.getLogger(NDCProvider.ENTERCONTEXT).fine("c1");
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2", new HashMap()});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 4);
        assertTrue(records.get(0).getParameters()[3] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[3]).get(NDCProvider.CONTEXT).toString().equals("c1"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).fine("c1");
        records.clear();
        
        
        // context: String[]; log: none
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().info("msg");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 1);
        assertTrue(records.get(0).getParameters()[0] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[0]).get("c1").toString().equals("c2"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        records.clear();
        
        // context: String[]; log: Object
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}", "1");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 2);
        assertTrue(records.get(0).getParameters()[1] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[1]).get("c1").toString().equals("c2"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        records.clear();
        
        // context: String[]; log: Object[]
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2"});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 3);
        assertTrue(records.get(0).getParameters()[2] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[2]).get("c3").toString().equals("c4"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        records.clear();
        
        // context: String[]; log: Object[], Map
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2", new HashMap()});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 4);
        assertTrue(records.get(0).getParameters()[3] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[3]).get("c3").toString().equals("c4"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}={1}, {2}={3}", new String[] {"c1", "c2", "c3", "c4"});
        records.clear();

        
        Map<String, String> m = new HashMap<String, String>();
        m.put("c1", "c2");
        m.put("c3", "c4");
        
        // context: Map; log: none
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}", m);
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().info("msg");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 1);
        assertTrue(records.get(0).getParameters()[0] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[0]).get("c1").toString().equals("c2"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}", m);
        records.clear();
        
        // context: Map; log: Object
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}", m);
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}", "1");
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 2);
        assertTrue(records.get(0).getParameters()[1] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[1]).get("c1").toString().equals("c2"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}", m);
        records.clear();
        
        // context: Map; log: Object[]
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}", m);
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2"});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 3);
        assertTrue(records.get(0).getParameters()[2] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[2]).get("c3").toString().equals("c4"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}", m);
        records.clear();
        
        // context: Map; log: Object[], Map
        Logger.getLogger(NDCProvider.ENTERCONTEXT).log(Level.FINE, "{0}", m);
        assertTrue(records.size() == 0);
        Logger.getAnonymousLogger().log(Level.INFO, "msg {0}{1}", new Object[] {"1", "2", new HashMap()});
        assertTrue(records.size() == 1);
        assertTrue(records.get(0).getParameters().length == 4);
        assertTrue(records.get(0).getParameters()[3] instanceof Map);
        assertTrue(((Map) records.get(0).getParameters()[3]).get("c3").toString().equals("c4"));
        Logger.getLogger(NDCProvider.EXITCONTEXT).log(Level.FINE, "{0}", m);
        records.clear();
    }
}
