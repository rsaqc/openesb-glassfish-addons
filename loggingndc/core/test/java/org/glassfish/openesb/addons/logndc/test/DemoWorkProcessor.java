/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.logndc.test;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



public class DemoWorkProcessor {
    private static Logger sLog = Logger.getLogger("com.sun.x.test.Test");
    private static Logger sEntry = Logger.getLogger("com.sun.EnterContext");
    private static Logger sExit = Logger.getLogger("com.sun.EnterContext");
    private static Localizer s18 = new Localizer();
    
    private String mApplicationName;
    
    public DemoWorkProcessor(String applicationName) {
        mApplicationName = applicationName;
    }
    
    /**
     * An onMessage() method that uses a simple context name 
     * 
     * @param o msg
     */
    public void onMessage0(Work o) {
        // Push context
        sEntry.log(Level.FINE, mApplicationName);
        
        try {
            // Do work
            o.getTask().run();

            // This will be logged with the context
            if (sLog.isLoggable(Level.FINE)) {
                sLog.fine("msg received from " + o.getSender());
            }
        } catch (Exception e) {
            // Handle exception here
            sLog.log(Level.SEVERE, s18.x("E001: Failed to process message from {0}. " 
                + "Message will be ignored. The error was: {1}", o.getSender(), e), e);
        } finally {
            // Pop context; make sure to use the same contextMap
            sExit.log(Level.FINE, "", mApplicationName);
        }
    }

    /**
     * An onMessage() method that creates a new Object[] for setting the context; the
     * overhead of creating the extra object is typically perfectly acceptable. 
     * 
     * @param o msg
     */
    public void onMessage1(Work o) {
        // Push context
        Object[] contextmap = new Object[] { "Context", mApplicationName, "MsgId", o.getMsgId()};
        sEntry.log(Level.FINE, "", contextmap);
        
        try {
            // Do work

            // This will be logged with the context
            if (sLog.isLoggable(Level.FINE)) {
                sLog.fine("msg received from " + o.getSender());
            }
        } catch (Exception e) {
            // Handle exception here
            sLog.log(Level.SEVERE, s18.x("E001: Failed to process message from {0}. " 
                + "Message will be ignored. The error was: {1}", o.getSender(), e), e);
        } finally {
            // Pop context; make sure to use the same contextMap
            sExit.log(Level.FINE, "", contextmap);
        }
    }
    
    /**
     * An onMessage() method that creates a new Map for setting the context; the
     * overhead of creating the extra object is typically perfectly acceptable. 
     * 
     * @param o msg
     */
    public void onMessage1a(Work o) {
        // Push context
        Map<String, String> contextmap = new HashMap<String, String>();
        contextmap.put("Context", mApplicationName);
        contextmap.put("MsgId", o.getMsgId());
        sEntry.log(Level.FINE, "", contextmap);
        
        try {
            // Do work

            // This will be logged with the context
            if (sLog.isLoggable(Level.FINE)) {
                sLog.fine("msg received from " + o.getSender());
            }
        } catch (Exception e) {
            // Handle exception here
            sLog.log(Level.SEVERE, s18.x("E001: Failed to process message from {0}. " 
                + "Message will be ignored. The error was: {1}", o.getSender(), e), e);
        } finally {
            // Pop context; make sure to use the same contextMap
            sExit.log(Level.FINE, "", contextmap);
        }
    }
    
    /**
     * Cached context map
     */
    Map<String, String> mContextMap = new HashMap<String, String>();
    
    /**
     * onMessage() method that uses a context map that is tied to the instance of this
     * object. This only works if there's only one thread at any given time accessing
     * the onMessage() method.
     * 
     * @param o msg
     */
    public void onMessage2(Work o) {
        // Push context
        mContextMap.put("Context", mApplicationName);
        mContextMap.put("MsgId", o.getMsgId());
        sEntry.log(Level.FINE, "", mContextMap);
        
        try {
            // Do work

            // This will be logged with the context
            if (sLog.isLoggable(Level.FINE)) {
                sLog.fine("msg received from " + o.getSender());
            }
        } catch (Exception e) {
            // Handle exception here
            sLog.log(Level.SEVERE, s18.x("E001: Failed to process message from {0}. " 
                + "Message will be ignored. The error was: {1}", o.getSender(), e), e);
        } finally {
            // Pop context; make sure to use the same contextMap
            sExit.log(Level.FINE, "", mContextMap);
        }
    }
    
    /**
     * Threadlocal cache of context maps to avoid creating Maps
     */
    private static ThreadLocal<Map<String, String>> sContextMap = new ThreadLocal<Map<String, String>>() {
        protected Map<String, String> initialValue() {
            return new HashMap<String, String>();
        }
    };
    
    /**
     * Example of an onmessage method that uses a threadlocal variable to cache context
     * maps to avoid object creation 
     * 
     * @param o msg
     */
    public void onMessage3(Work o) {
        // Push context
        sContextMap.get().put("Context", mApplicationName);
        sContextMap.get().put("MsgId", o.getMsgId());
        sEntry.log(Level.FINE, "", sContextMap.get());
        
        try {
            // Do work

            // This will be logged with the context
            if (sLog.isLoggable(Level.FINE)) {
                sLog.fine("msg received from " + o.getSender());
            }
        } catch (Exception e) {
            // Handle exception here
            sLog.log(Level.SEVERE, s18.x("E001: Failed to process message from {0}. " 
                + "Message will be ignored. The error was: {1}", o.getSender(), e), e);
        } finally {
            // Pop context; make sure to use the same contextMap
            sExit.log(Level.FINE, "", sContextMap.get());
        }
    }
    

    /**
     * A localization tool, just for illustration 
     */
    private static class Localizer {
        public String x(String msg, Object... args) {
            return null;
        }
    }
    
    /**
     * Illustration purposes only
     */
    public interface Work {
        public String getMsgId();
        public String getSender();
        public Runnable getTask();
    }
}
