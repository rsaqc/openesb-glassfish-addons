/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.ConnectorType;
import org.glassfish.openesb.addons.appverifier.j2ee.sunra.descriptor.SunConnector;
import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.ResourceAdapter;

import java.io.ByteArrayInputStream;
import java.io.File;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.management.MBeanServer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/**
 * RAHelper is used to read ra.xml / sun-ra.xml and validate.
 * @author Sreeni Genipudi
 */
public class RAHelper {
    //private Unmarshaller sunRAUnm = null;
    private Unmarshaller raUnm = null;
    private static RAHelper mInstance = null;
    private static ErrorBundle mEb = null;
    private static ValidationConfig mVc = null;
    private static Map<String, String> mMapJNDINameConnIntf = null;
    private static List<String> mResourceMIDList = null;
    private GlobalRARVerifier mGrv = null;

    private RAHelper(MBeanServer mbserver, String instance) {
        try {
            //    JAXBContext sunRACtx = JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.j2ee.sunra.descriptor");

            //      this.sunRAUnm = sunRACtx.createUnmarshaller();
            //    this.sunRAUnm.setSchema(null);

            JAXBContext raCtx = 
                JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor");
            this.raUnm = raCtx.createUnmarshaller();

            if (this.mVc.checkValidGlobalRAR()) {
                try {
                    if (mbserver == null) {
                        mGrv = 
new GlobalRARVerifier(mVc.getHostName(), mVc.getJMXPort(), mVc.getUser(), 
                      mVc.getPassword(), instance);
                    } else {
                        mGrv = new GlobalRARVerifier(mbserver, instance);
                    }
                    String[] arryGlobalRARs = mGrv.getAllGlobalRARJNDINames();
                    Object[] objArry = 
                        mGrv.getAllJNDINamesWithConnectionTypeAndMIDs(arryGlobalRARs);
                    mMapJNDINameConnIntf = (Map<String, String>)objArry[0];
                    mResourceMIDList = (ArrayList<String>)objArry[1];

                } catch (Exception e) {
                    mEb.addError("Error while instantiating RAHelper ", e);
                }
            }
        } catch (JAXBException jxe) {

        }
    }

    public boolean isGlobalResource(String jndiName, String connectionIntg, 
                                    ErrorBundle eb) {
        if (!this.mVc.checkValidGlobalRAR()) {
            return true;
        }
        String conIntf = mMapJNDINameConnIntf.get(jndiName);
        if (conIntf == null) {
            if (!this.mGrv.isValidJNIName(jndiName)) {
                eb.addError(" Invalid JNDI name = " + jndiName);
                return false;
            } else {
                return true;
            }
        }
        if (connectionIntg != null && !connectionIntg.equals(conIntf)) {
            eb.addError("For JNDI Name - " + jndiName + 
                        "Connection interface does not match.Expected " + 
                        conIntf + " but found " + connectionIntg);
            return false;
        }
        return true;
    }

    public boolean isGlobalResourceMID(String resourceMID) {
        if (!this.mVc.checkValidGlobalRAR()) {
            return true;
        }
        return this.mResourceMIDList.contains(resourceMID);
    }

    public static RAHelper getInstance(ValidationConfig cfg, ErrorBundle eb, 
                                       String instance) {
        mEb = eb;
        mVc = cfg;
        if (mInstance == null) {
            mInstance = new RAHelper(null, instance);
        }
        return mInstance;
    }


    public static RAHelper getInstance(ValidationConfig cfg, ErrorBundle eb, 
                                       MBeanServer mbServer, String instance) {
        mEb = eb;
        mVc = cfg;

        if (mInstance == null) {
            mInstance = new RAHelper(mbServer, instance);
        }
        ;

        try {
            GlobalRARVerifier grv = new GlobalRARVerifier(mbServer, instance);
            String[] arryGlobalRARs;
            arryGlobalRARs = grv.getAllGlobalRARJNDINames();
            Object[] objArry = 
                grv.getAllJNDINamesWithConnectionTypeAndMIDs(arryGlobalRARs);
            mMapJNDINameConnIntf = (Map<String, String>)objArry[0];
            mResourceMIDList = (ArrayList<String>)objArry[1];
        } catch (Exception e) {
            // TODO
        }
        return mInstance;


    }

    ResourceAdapter getResourceAdapter(String earFileName, String raDesc, 
                                       String raXML, String sunRAXML, 
                                       String raFileName, 
                                       ValidationConfig config) {
        ErrorBundle eb = this.mEb;
        javax.xml.bind.JAXBElement je = null;
        String jndiName = null;
        if (sunRAXML != null) {
            jndiName = this.getJNDIName(sunRAXML);
        }
        //Process SunRAXML
        File raFile = new File(raXML);
        //EjbJarType ej = (EjbJarType)
        if (this.raUnm == null) {
            JAXBContext raCtx = null;
            try {
                raCtx = 
                        JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor");
            } catch (JAXBException e) {
                mEb.addError("Error while instantiating RA Descriptor model ", 
                             e);
            }
            try {
                this.raUnm = raCtx.createUnmarshaller();
            } catch (JAXBException e) {
                mEb.addError("Error while instantiating RA Descriptor model ", 
                             e);
            }
        }

        try {
            ByteArrayInputStream bis = 
                new ByteArrayInputStream(raXML.getBytes());
            je = (javax.xml.bind.JAXBElement)this.raUnm.unmarshal(bis);
        } catch (JAXBException e) {
            eb.addError(" Error while reading sun ra.xml file" + raXML, e);
        }
        ResourceAdapter resAdap = null;
        ConnectorType conType = (ConnectorType)je.getValue();
        org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.ResourceadapterType resAdapType = 
            conType.getResourceadapter();
        String appName = earFileName;
        String raName = getFileName(raFileName);
        org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.InboundResourceadapterType inResAdap = 
            resAdapType.getInboundResourceadapter();
        ArrayList<String> listOfMIDs = new ArrayList<String>();
        if (inResAdap != null) {
            InboundResourceAdapter inRA = new InboundResourceAdapter();
            inRA.setJNDIName(null);
            inRA.setRAFileName(raXML, sunRAXML);
            listOfMIDs.add(appName + "#" + raName);

            org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.MessageadapterType messType = 
                inResAdap.getMessageadapter();
            java.util.List<org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.MessagelistenerType> msgListenerList = 
                messType.getMessagelistener();
            List<String> listOfMessageListeners = new ArrayList<String>();
            for (org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.MessagelistenerType ml: 
                 msgListenerList) {
                listOfMessageListeners.add(ml.getMessagelistenerType().getValue());
                listOfMIDs.add(appName + "#" + raName + "#" + 
                               ml.getMessagelistenerType().getValue());
            }
            inRA.setMessageListenerType(listOfMessageListeners.toArray(new String[0]));
            inRA.setMID(listOfMIDs.toArray(new String[0]));
            return inRA;
        } else {
            OutboundResourceAdapter outResAdapWrap = 
                new OutboundResourceAdapter();
            org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.OutboundResourceadapterType outResAdap = 
                resAdapType.getOutboundResourceadapter();
            java.util.List<org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.ConnectionDefinitionType> outConList = 
                outResAdap.getConnectionDefinition();
            List<String> connectionIntfList = new ArrayList<String>();
            for (org.glassfish.openesb.addons.appverifier.j2ee.ra.descriptor.ConnectionDefinitionType outConn: 
                 outConList) {
                connectionIntfList.add(outConn.getConnectionfactoryInterface().getValue());
            }
            outResAdapWrap.setConnectionInterface(connectionIntfList.toArray(new String[0]));
            outResAdapWrap.setRAFileName(raFileName, sunRAXML);
            ArrayList<String> jndiNameList = new ArrayList<String>();
            if (jndiName == null || jndiName.equals("")) {
                for (String conIntd: connectionIntfList) {
                    jndiNameList.add(appName + "#" + raName + "#" + conIntd);
                    jndiNameList.add("__SYSTEM/resource/" + appName + "#" + 
                                     raName + "#" + conIntd);
                }
            } else {
                jndiNameList.add(jndiName);
            }
            outResAdapWrap.setJNDIName(jndiNameList.toArray(new String[0]));
            return outResAdapWrap;
        }


    }

    private String getFileName(String fileName) {
        File earFile = new File(fileName);
        String appName = earFile.getName();
        int lInx = appName.indexOf(".");
        if (lInx >= 0) {
            appName = appName.substring(0, lInx);
        }
        return appName;
    }

    private String getJNDIName(String sunRAXML) {
        String jndiName = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //factory.setNamespaceAware(true);
        //factory.setValidating(true); 

        try {
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                // TODO
            }
            ByteArrayInputStream bis = 
                new ByteArrayInputStream(sunRAXML.getBytes());

            Document document = builder.parse(bis);
            NodeList nl = document.getElementsByTagName("resource-adapter");
            Node resourceAdapterNode = nl.item(0);
            NamedNodeMap nnMap = resourceAdapterNode.getAttributes();
            Node jndiNameNode = nnMap.getNamedItem("jndi-name");
            jndiName = jndiNameNode.getNodeValue();
        } catch (SAXException e) {
            // TODO
        } catch (IOException e) {
            // TODO
        }
        return jndiName;
    }


}

