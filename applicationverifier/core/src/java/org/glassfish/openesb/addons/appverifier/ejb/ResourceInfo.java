/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.ejb;

import org.glassfish.openesb.addons.appverifier.VerifyStatus;

/**
 * Metadata for resource
 * @author Sreeni Genipudi
 */
public class ResourceInfo {
    // Resource reference name
    private String mResourceRefName;
    //Resource type
    private String mResourceType;
    //Resource jndi name
    private String mResourceJNDIName;

    private String mMessage = "";
    //Verification status
    private VerifyStatus mStatus = VerifyStatus.OK;

    /**
     * Set Resource reference name
     * @param resRefName
     */
    public void setResourceRefName(String resRefName) {
        mResourceRefName = resRefName;
    }

    /**
     * Get resource reference name
     * @return resource reference name
     */
    public String getResRefName() {
        return mResourceRefName;
    }

    /**
     * Set resource type
     * @param resource type
     */
    public void setResourceType(String typ) {
        mResourceType = typ;
    }

    /**
     * Get resource type
     * @return resource type
     */
    public String getResourceType() {
        return mResourceType;
    }

    /**
     * Set resource JNDI name
     * @param jndi name
     */
    public void setResourceJNDIName(String jndi) {
        this.mResourceJNDIName = jndi;
    }

    /**
     * Get resource jndi name
     * @return resource jndi name
     */
    public String getResJNDIName() {
        return mResourceJNDIName;
    }


    /**
     * Set message
     * @param message
     */
    public void setMessage(String message) {
        this.mMessage = message;
    }

    /**
     * Get message
     * @return
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * Set status 
     * @param status
     */
    public void setStatus(VerifyStatus status) {
        this.mStatus = status;
    }

    /**
     * Get status
     * @return status
     */
    public VerifyStatus getStatus() {
        return this.mStatus;
    }


}
