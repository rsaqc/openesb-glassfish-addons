/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.ejb;

import org.glassfish.openesb.addons.appverifier.ApplicationReader;
import org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ApplicationType;

import java.io.ByteArrayInputStream;
import java.io.File;

import java.io.StringReader;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * This class is used to read the application.xml of JAVAEE EAR file.
 * @author Sreeni Genipudi
 */
public class ApplicationJavaEEReader implements ApplicationReader {
    //List of ejb names
    private List<String> mEJBList = new ArrayList<String>();
    //List of RAR names
    private List<String> mRAList = new ArrayList<String>();
    //List of WAR file names
    private List<String> mWARList = new ArrayList<String>();

    public ApplicationJavaEEReader() {
    }
    /*
     * Constructor
     * @Param application xml content
     */

    public ApplicationJavaEEReader(String applicationXML) throws Exception {

        StringReader strRead = new StringReader(applicationXML);
        JAXBContext jc = 
            JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.javaee.application.descriptor");
        Unmarshaller um = jc.createUnmarshaller();
        //EjbJarType ej = (EjbJarType)
        javax.xml.bind.JAXBElement je = 
            (javax.xml.bind.JAXBElement)um.unmarshal(strRead);
        ApplicationType at = (ApplicationType)je.getValue();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ModuleType> moduleType = 
            at.getModule();
        for (org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ModuleType mt: 
             moduleType) {
            org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PathType pt = 
                mt.getEjb();
            if (pt != null) {
                this.mEJBList.add(pt.getValue());
            } else {
                pt = mt.getConnector();
                if (pt != null) {
                    this.mRAList.add(pt.getValue());
                } else {
                    org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.WebType wt = 
                        mt.getWeb();
                    if (wt != null) {
                        this.mWARList.add(wt.getId());
                    }
                }
            }
        }

    }

    /*
     * Constructor
     * @Param application xml File instance
     */

    public ApplicationJavaEEReader(File applicationXML) throws Exception {

        JAXBContext jc = 
            JAXBContext.newInstance("org.glassfish.openesb.addons.appverifier.javaee.application.descriptor");
        Unmarshaller um = jc.createUnmarshaller();
        //EjbJarType ej = (EjbJarType)
        javax.xml.bind.JAXBElement je = 
            (javax.xml.bind.JAXBElement)um.unmarshal(applicationXML);
        ApplicationType at = (ApplicationType)je.getValue();
        java.util.List<org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ModuleType> moduleType = 
            at.getModule();
        for (org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ModuleType mt: 
             moduleType) {
            org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PathType pt = 
                mt.getEjb();
            if (pt != null) {
                this.mEJBList.add(pt.getValue());
            } else {
                pt = mt.getConnector();
                if (pt != null) {
                    this.mRAList.add(pt.getValue());
                } else {
                    org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.WebType wt = 
                        mt.getWeb();
                    if (wt != null) {
                        this.mWARList.add(wt.getId());
                    }
                }
            }
        }

    }

    /**
     * Get list of RA
     * @return
     */
    public List<String> getRAList() {
        return this.mRAList;
    }

    /**
     * Get list of EJB
     * @return
     */
    public List<String> getEJBList() {
        return this.mEJBList;
    }

    /**
     * Get list of War
     * @return
     */
    public List<String> getWebList() {
        return this.mWARList;
    }
}

