/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

/**
 * Error Bundle handler
 * @author Sreeni Genipudi
 */
public class ErrorBundle {
    //holds error messages 
    private StringBuffer mErrorMessages = new StringBuffer(20);
    //holds warning message
    private StringBuffer mWarningMessages = new StringBuffer(20);
    //holds informational message
    private StringBuffer mInfoMessages = new StringBuffer(20);
    /**
     * Constructor
     */
    public ErrorBundle() {
    }
    
    /**
     * Add Warning
     * @param message
     */
    public void addWarning(String message) {
        this.addWarning(message, null);
    }

    /**
     * Add Warning
     * @param message
     * @param ex Exception
     */
    public void addWarning(String message, Exception ex) {
        mWarningMessages.append(message);
        mWarningMessages.append("\n");
        if (ex != null) {
            mWarningMessages.append(ex.toString());
            mWarningMessages.append("\n");
        }
    }

    /**
     * Add informational message
     * @param message
     */
    public void addInfo(String message) {
        mInfoMessages.append(message);
        mInfoMessages.append("\n");
    }

    /**
     * Add Error message
     * @param message
     */
    public void addError(String message) {
        this.addError(message, null);
    }

    /**
     * Add Error message
     * @param message
     * @param ex
     */
    public void addError(String message, Exception ex) {
        mErrorMessages.append(message);
        mErrorMessages.append("\n");
        if (ex != null) {
            mErrorMessages.append(ex.toString());
            mErrorMessages.append("\n");
        }
    }

    /**
     * Get informational message
     * @return informational message
     */
    public StringBuffer getInfo() {
        return this.mInfoMessages;

    }

    /**
     * Get warning message
     * @return warning message
     */
    public StringBuffer getWarning() {
        return this.mWarningMessages;
    }

    /**
     * Get error message
     * @return error message
     */
    public StringBuffer getError() {
        return this.mErrorMessages;
    }

}
