/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.validation;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;

import org.glassfish.openesb.addons.appverifier.PropertyInfo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Validation Helper 
 * @author Sreeni Genipudi
 */
public class ValidationHelper {
    //Validation helper instance
    private static ValidationHelper mInstance = null;
    //Resource bundle
    private ResourceBundle mRb = null;
    //Constants
    private static final String ERROR_INSTANTIATE_CLASS = 
        "ERROR_INSTANTIATE_CLASS";
    private static final String ERROR_LOADING_CLASS = "ERROR_LOADING_CLASS";
    private static final String ERROR_SET_PROPERTY_NO_SUITABLE_CONSTRUCTOR = 
        "ERROR_SET_PROPERTY_NO_SUITABLE_CONSTRUCTOR";
    private static final String ERROR_FIND_METHOD_FOR_PROPERTY = 
        "ERROR_FIND_METHOD_FOR_PROPERTY";
    private static final String ERROR_INSTANTIATE_CLASS_SET_PROPERTY = 
        "ERROR_INSTANTIATE_CLASS_SET_PROPERTY";
    private static final String ERROR_SET_PROPERTY = "ERROR_SET_PROPERTY";
    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.Validation.ValidationHelper");
    private static final String SET_PREFIX = "set";

    /**
     * Constructor.
     */
    private ValidationHelper() {
        mRb = 
ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.validation.Bundle", 
                         Locale.getDefault());
    }

    public static ValidationHelper getInstance() {
        if (mInstance == null) {
            mInstance = new ValidationHelper();
        }
        return mInstance;
    }


    public boolean CheckClassExistence(String className, ClassLoader loader, 
                                       ErrorBundle errorBundle) {
        /*    try {
            if (loader.loadClass(className) == null) {
                errorBundle.addError(mRb.getString(ERROR_INSTANTIATE_CLASS)+className);
                return false;
            }
        }catch (Exception ex) {
            errorBundle.addError(mRb.getString(ERROR_INSTANTIATE_CLASS)+className,ex);
            return false;
        }
        logger.fine(" Successfully Checked CLASS EXISTENCE FOR "+className);*/
        return true;
    }

    public boolean CheckSetProperties(Class classObj, ClassLoader loader, 
                                      ErrorBundle errorBundle, 
                                      List<PropertyInfo> propList) {
        try {
            Object classInstance = classObj.newInstance();
            Class propertyClass = null;
            Constructor cons = null;
            Object propObj = null;
            Method m = null;
            String propertyName = null;
            for (PropertyInfo propInfo: propList) {
                propertyName = propInfo.getPropertyName();
                try {
                    propertyClass = Class.forName(propInfo.getPropertyType());
                } catch (Exception propEx) {
                    errorBundle.addError(mRb.getString(ERROR_LOADING_CLASS) + 
                                         propInfo.getPropertyType(), propEx);
                    continue;
                }
                try {
                    cons = 
propertyClass.getConstructor(new Class[] { java.lang.String.class });
                } catch (Exception propEx) {
                    errorBundle.addWarning(mRb.getString(ERROR_SET_PROPERTY_NO_SUITABLE_CONSTRUCTOR) + 
                                           propInfo.getPropertyType(), propEx);
                    continue;
                }
                try {
                    propObj = cons.newInstance(propInfo.getPropertyValue());
                } catch (Exception propEx) {
                    errorBundle.addError(mRb.getString(ERROR_INSTANTIATE_CLASS_SET_PROPERTY) + 
                                         propInfo.getPropertyType() + "/" + 
                                         propInfo.getPropertyValue(), propEx);
                    continue;
                }
                try {
                    m = 
  classObj.getMethod(SET_PREFIX + propertyName, new Class[] { propertyClass });
                } catch (Exception propEx) {
                    errorBundle.addError(mRb.getString(ERROR_FIND_METHOD_FOR_PROPERTY) + 
                                         propInfo.getPropertyName() + "/" + 
                                         classObj.getName(), propEx);
                }
                try {
                    m.invoke(classInstance, new Object[] { propObj });
                } catch (Exception propEx) {
                    errorBundle.addError(mRb.getString(ERROR_SET_PROPERTY) + 
                                         propInfo.getPropertyName() + "/" + 
                                         classObj.getName() + "/" + 
                                         propInfo.getPropertyValue(), propEx);
                }
            }
        } catch (Exception ex) {
            errorBundle.addError(mRb.getString(ERROR_INSTANTIATE_CLASS) + 
                                 classObj.getName(), ex);
            return false;
        }
        return true;
    }

}
