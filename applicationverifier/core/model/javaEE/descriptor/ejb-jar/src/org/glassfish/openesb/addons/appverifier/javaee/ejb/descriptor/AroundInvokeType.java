/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-382 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2007.10.16 at 11:21:21 PM PDT 
//


package org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 
 *         The around-invoke type specifies a method on a
 *         class to be called during the around invoke portion of an
 *         ejb invocation.  Note that each class may have only one
 *         around invoke method and that the method may not be
 *         overloaded.
 * 
 *         If the class element is missing then
 *         the class defining the callback is assumed to be the
 *         interceptor class or component class in scope at the
 *         location in the descriptor in which the around invoke
 *         definition appears.
 * 
 *       
 * 
 * <p>Java class for around-invokeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="around-invokeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="class" type="{http://java.sun.com/xml/ns/javaee}fully-qualified-classType" minOccurs="0"/>
 *         &lt;element name="method-name" type="{http://java.sun.com/xml/ns/javaee}java-identifierType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "around-invokeType", propOrder = { "clazz", "methodName" })
public class AroundInvokeType {

    @XmlElement(name = "class")
    protected FullyQualifiedClassType clazz;
    @XmlElement(name = "method-name", required = true)
    protected JavaIdentifierType methodName;

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     * possible object is
     * {@link org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.FullyQualifiedClassType}
     * 
     */
    public FullyQualifiedClassType getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     * allowed object is
     * {@link org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.FullyQualifiedClassType}
     * 
     */
    public void setClazz(FullyQualifiedClassType value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the methodName property.
     * 
     * @return
     * possible object is
     * {@link org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.JavaIdentifierType}
     * 
     */
    public JavaIdentifierType getMethodName() {
        return methodName;
    }

    /**
     * Sets the value of the methodName property.
     * 
     * @param value
     * allowed object is
     * {@link org.glassfish.openesb.addons.appverifier.javaee.ejb.descriptor.JavaIdentifierType}
     * 
     */
    public void setMethodName(JavaIdentifierType value) {
        this.methodName = value;
    }

}
