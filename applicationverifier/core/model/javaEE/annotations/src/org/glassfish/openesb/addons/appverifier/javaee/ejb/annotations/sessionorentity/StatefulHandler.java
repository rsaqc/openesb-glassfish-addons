/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.sessionorentity;


import org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;
import org.glassfish.openesb.addons.appverifier.ValidationConfig;
import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.EJBAnnotationsHandler;

import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;

import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.netbeans.modules.classfile.ClassFile;

/**
 * Stateful handler
 * @author Sreeni Genipudi
 */
public class StatefulHandler implements EJBAnnotationsHandler {


    public StatefulHandler() {
    }


    public Map<String, Map<String, Object>> createMap() {
        Map<String, Object> mapOfAttrName = new HashMap<String, Object>();
        mapOfAttrName.put(EJBAnnotationsHandler.EJB_NAME, null);
        mapOfAttrName.put(EJBAnnotationsHandler.EJB_MAPPED_NAME, null);
        Map<String, Map<String, Object>> ejbAnnotationsMap = 
            new HashMap<String, Map<String, Object>>();
        ejbAnnotationsMap.put("javax.ejb.Stateful", mapOfAttrName);
        // return mapOfAttrName;
        //TODO:
        return null;
    }


    public EJBInfo process(Map<String, Map<String, Object>> annotationsMap, 
                           List<InboundResourceAdapter> inbound, 
                           List<OutboundResourceAdapter> out, 
                           SunEjbJar sunEjbJarDesc, ValidationConfig vc, 
                           ClassLoader loader, String className, 
                           ClassFile classFile, ErrorBundle errorBundle, 
                           String instance) {
        return null;
    }
}
