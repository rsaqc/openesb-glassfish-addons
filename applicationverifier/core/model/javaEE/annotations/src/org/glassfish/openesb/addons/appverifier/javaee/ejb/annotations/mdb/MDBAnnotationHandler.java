/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.mdb;

import org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;
import org.glassfish.openesb.addons.appverifier.OutboundRARValidator;
import org.glassfish.openesb.addons.appverifier.RAHelper;
import org.glassfish.openesb.addons.appverifier.ValidationConfig;
import org.glassfish.openesb.addons.appverifier.VerifyStatus;
import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EJBReferenceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EjbJavaEEXMLReader;
import org.glassfish.openesb.addons.appverifier.ejb.MDBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.ResourceInfo;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.AnnotationsReader;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.EJBAnnotationsHandler;


import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;


import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ElementValue;
import org.netbeans.modules.classfile.NestedElementValue;
import org.netbeans.modules.classfile.PrimitiveElementValue;

/**
 * MDB Annotation handler. Parses the MDB Based annotations in classfiles 
 * and produces EJB metadata information and validates resource validation
 * @author Sreeni Genipudi
 */
public class MDBAnnotationHandler implements EJBAnnotationsHandler {
    private ResourceBundle mRb = 
        ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.ejb.Bundle", 
                                 Locale.getDefault());
    public static final String MDB_MESSAGE_LISTENER_INTERFACE = 
        "messageListenerInterface ";
    public static final String MDB_ACTIVATION_CONFIG = "activationConfig";
    private final ArrayList<String> mExcludeClasses = new ArrayList<String>();

    public MDBAnnotationHandler() {
    }


    public Map<String, Map<String, Object>> createMap() {

        Map<String, Map<String, Object>> mdbAnnotationsMap = 
            new HashMap<String, Map<String, Object>>();

        //1. EJB MAP 
        Map<String, Object> ejbAttrMap = new HashMap<String, Object>();
        ejbAttrMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        ejbAttrMap.put(MDB_MESSAGE_LISTENER_INTERFACE, null);
        ejbAttrMap.put(EJBAnnotationsHandler.EJB_MAPPED_NAME, null);
        ejbAttrMap.put(MDB_ACTIVATION_CONFIG, null);
        mdbAnnotationsMap.put("javax.ejb.MessageDriven", ejbAttrMap);

        // 2. Resource Reference 
        Map<String, Object> resRefMap = new HashMap<String, Object>();
        resRefMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        resRefMap.put(EJBAnnotationsHandler.RESOURCE_MAPPED_NAME, null);
        resRefMap.put(EJBAnnotationsHandler.RESOURCE_DESCRIPTION, null);
        resRefMap.put(EJBAnnotationsHandler.RESOURCE_ANN_TYPE, Class.class);
        mdbAnnotationsMap.put("javax.annotation.Resource", resRefMap);


        // 3. EJB References 
        Map<String, Object> ejbRefMap = new HashMap<String, Object>();
        ejbRefMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        ejbRefMap.put(EJBAnnotationsHandler.RESOURCE_MAPPED_NAME, null);
        ejbRefMap.put(EJBAnnotationsHandler.EJB_REFERENCE_BEANINFO, 
                      Object.class);
        ejbRefMap.put(EJBAnnotationsHandler.EJB_REFERENCE_BEANNAME, 
                      Object.class);
        mdbAnnotationsMap.put("javax.ejb.EJB", ejbRefMap);


        mExcludeClasses.add("java.io.Serializable");
        mExcludeClasses.add("java.io.Externalizable");
        mExcludeClasses.add("javax.ejb.EJBContext");
        mExcludeClasses.add("javax.ejb.EJBHome");
        mExcludeClasses.add("javax.ejb.EJBLocalHome");
        mExcludeClasses.add("javax.ejb.EJBLocalObject");
        mExcludeClasses.add("javax.ejb.EJBMetaData");
        mExcludeClasses.add("javax.ejb.EJBObject");
        mExcludeClasses.add("javax.ejb.EnterpriseBean");
        mExcludeClasses.add("javax.ejb.EntityBean");
        mExcludeClasses.add("javax.ejb.EntityContext");
        mExcludeClasses.add("javax.ejb.Handle");
        mExcludeClasses.add("javax.ejb.HomeHandle");
        mExcludeClasses.add("javax.ejb.MessageDrivenBean");
        mExcludeClasses.add("javax.ejb.MessageDrivenContext");
        mExcludeClasses.add("javax.ejb.SessionBean");
        mExcludeClasses.add("javax.ejb.SessionContext");
        mExcludeClasses.add("javax.ejb.SessionSynchronization");
        mExcludeClasses.add("javax.ejb.TimedObject");
        mExcludeClasses.add("javax.ejb.Timer");
        mExcludeClasses.add("javax.ejb.TimerHandle");
        mExcludeClasses.add("javax.ejb.TimerService");
        return mdbAnnotationsMap;
    }

    /**
     * Process the Annotations in the EJB class. Called from ClassLoaderUtil.getEJBInfo().
     * 
     * @param annotationsMap
     * @param inbound
     * @param out
     * @param sunEjbJarDesc
     * @param vc
     * @param loader
     * @param className
     * @param classFile
     * @param errorBundle
     * @param instance
     * @return
     */
    public EJBInfo process(Map<String, Map<String, Object>> annotationsMap, 
                           List<InboundResourceAdapter> inbound, 
                           List<OutboundResourceAdapter> out, 
                           SunEjbJar sunEjbJarDesc, ValidationConfig vc, 
                           ClassLoader loader, String className, 
                           ClassFile classFile, ErrorBundle errorBundle, 
                           String instance) {
        //process annotations.
        MDBInfo mdbInfo = new MDBInfo(vc, loader, errorBundle);
        mdbInfo.setEJBClassName(className.replace('/', '.'));
        Map<String, Object> msgBeanAnnotationMap = 
            annotationsMap.get("javax.ejb.MessageDriven");
        Object propValueObj = null;
        propValueObj = 
                msgBeanAnnotationMap.get(EJBAnnotationsHandler.EJB_NAME);
        AnnotationsReader annRdr = AnnotationsReader.getInstance();
        String mdbName = annRdr.getPrimitiveValue(propValueObj);
        propValueObj = 
                msgBeanAnnotationMap.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
        RAHelper raHlpr = RAHelper.getInstance(vc, errorBundle, instance);
        String mdbJndiName = annRdr.getPrimitiveValue(propValueObj);
        List<ResourceInfo> listOfResources = null;
        listOfResources = new ArrayList<ResourceInfo>();
        propValueObj = 
                msgBeanAnnotationMap.get(MDB_MESSAGE_LISTENER_INTERFACE);
        String mdbListenerIntf = annRdr.getPrimitiveValue(propValueObj);

        org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.AnnotationsReader annReader = 
            null;
        Class tagName = null;
        if (mdbListenerIntf == null) {
            annReader = 
                    org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.AnnotationsReader.getInstance();
            //Class mdbClass = Class.forName(className);
            ClassName mdbCN = 
                ClassName.getClassName(className.replace('.', '/'));
            ClassName msgType = 
                annReader.getInterface(classFile, mExcludeClasses, tagName, 
                                       errorBundle, loader);
            if (msgType != null) {
                mdbListenerIntf = msgType.getExternalName();
                mdbInfo.setMessagingType(mdbListenerIntf);
            }
        }

        if (mdbJndiName != null && 
            !raHlpr.isGlobalResource(mdbJndiName, null, errorBundle)) {
            ResourceInfo resInfo = new ResourceInfo();
            resInfo.setStatus(VerifyStatus.ERROR);
            resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                               " " + mdbJndiName + 
                               ((mdbListenerIntf != null) ? " " + 
                                mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                mdbListenerIntf : ""));
            listOfResources.add(resInfo);

        }


        propValueObj = 
                msgBeanAnnotationMap.get(EJBAnnotationsHandler.EJB_CLASS_NAME);
        String mdbClassName = annRdr.getPrimitiveValue(propValueObj);
        if (mdbName == null) {
            if (mdbClassName == null) {
                mdbClassName = className;
                mdbClassName.replace('.', '/');
                int nameIndx = mdbClassName.lastIndexOf("/");
                if (nameIndx > 0) {
                    mdbClassName = className.substring(nameIndx + 1);
                }
            }
            mdbName = mdbClassName;
        }
        mdbInfo.setEJBName(mdbName);
        if (mdbJndiName != null) {
            mdbInfo.setJNDIName(mdbJndiName);
        }


        Object activConfigPropertiesObj = 
            msgBeanAnnotationMap.get(MDB_ACTIVATION_CONFIG);
        if (activConfigPropertiesObj != null && 
            activConfigPropertiesObj instanceof 
            org.netbeans.modules.classfile.ArrayElementValue) {
            org.netbeans.modules.classfile.ArrayElementValue activConfigProperties = 
                (org.netbeans.modules.classfile.ArrayElementValue)activConfigPropertiesObj;
            Map mapOfActivationConfig = new HashMap<String, String>();
            processActivationConfigAnnotation(activConfigProperties, 
                                              mapOfActivationConfig);
            mdbInfo.setActivationConfig(mapOfActivationConfig);
        }

        //Process Resource
        Map<String, Object> resRefAnnotationMap = 
            annotationsMap.get("javax.annotation.Resource");
        ResourceInfo resInfo = null;
        String resourceJNDIName = null;
        String resourceType = null;
        String resourceName = null;
        String resDescr = null;
        OutboundRARValidator outRARValidator = 
            OutboundRARValidator.getInstance();


        String resName = 
            (String)resRefAnnotationMap.get(EJBAnnotationsHandler.EJB_NAME);
        String resMapName = null;


        if (resName != null) {
            resInfo = new ResourceInfo();
            listOfResources.add(resInfo);
            resInfo.setResourceJNDIName(resName);

            resMapName = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
            if (resMapName == null) {
                resInfo.setResourceRefName(resName);
            }

            resourceType = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.RESOURCE_ANN_TYPE);
            if (resourceType != null) {
                resInfo.setResourceType(resourceType);
            }


            resDescr = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.RESOURCE_DESCRIPTION);
            resDescr = (resDescr != null) ? resDescr : "";

            if (outRARValidator.isResourceLocal(resMapName)) {
                if (!outRARValidator.isValidLocalResource(resMapName, 
                                                          resourceType, out, 
                                                          errorBundle)) {
                    resInfo.setStatus(VerifyStatus.ERROR);
                    resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_LOCAL_RESOURCE) + 
                                       " " + resMapName + " " + 
                                       mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                       resourceType + " " + resDescr);
                }
            } else {
                if (!raHlpr.isGlobalResource(resMapName, resourceType, 
                                             errorBundle)) {
                    resInfo.setStatus(VerifyStatus.ERROR);
                    resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                                       " " + resMapName + " " + 
                                       mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                       resourceType + " " + resDescr);

                }
            }

        }

        ArrayList<Properties> resProp = 
            (ArrayList<Properties>)resRefAnnotationMap.get(AnnotationsReader.FIELD_LIST);
        if (resProp != null) {
            for (Properties resourceProp: resProp) {
                resInfo = new ResourceInfo();
                resInfo.setMessage("");
                resInfo.setStatus(VerifyStatus.OK);
                resourceJNDIName = 
                        (String)resourceProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (resourceJNDIName == null) {
                    resourceJNDIName = 
                            (String)resourceProp.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
                }

                resourceName = 
                        (String)resourceProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (resourceName == null) {
                    resourceName = resourceJNDIName;
                }
                if (resourceJNDIName == null) {
                    resourceJNDIName = resourceName;
                }

                if (resourceJNDIName != null) {
                    listOfResources.add(resInfo);
                } else {
                    continue;
                }
                resInfo.setResourceRefName(resourceName);
                resInfo.setResourceJNDIName(resourceJNDIName);

                resourceType = 
                        (String)resourceProp.get(EJBAnnotationsHandler.RESOURCE_ANN_TYPE);
                if (resourceType == null) {
                    resourceType = 
                            (String)resourceProp.get(AnnotationsReader.CLASSNAME);
                }
                resInfo.setResourceType(resourceType);
                resDescr = 
                        (String)resourceProp.get(EJBAnnotationsHandler.RESOURCE_DESCRIPTION);
                resDescr = (resDescr != null) ? resDescr : "";
                if (outRARValidator.isResourceLocal(resourceJNDIName)) {
                    if (!outRARValidator.isValidLocalResource(resourceJNDIName, 
                                                              resourceType, 
                                                              out, 
                                                              errorBundle)) {
                        resInfo.setStatus(VerifyStatus.ERROR);
                        resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_LOCAL_RESOURCE) + 
                                           " " + resourceJNDIName + " " + 
                                           mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                           resourceType + " " + resDescr);
                    }
                } else {
                    if (!raHlpr.isGlobalResource(resourceJNDIName, 
                                                 resourceType, errorBundle)) {
                        resInfo.setStatus(VerifyStatus.ERROR);
                        resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                                           " " + resourceJNDIName + " " + 
                                           mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                           resourceType + " " + resDescr);
                    }
                }

            }
        }
        mdbInfo.setResourceReferenceInfo(listOfResources);

        //EJBREFS
        Map<String, Object> ejbRefAnnotationMap = 
            annotationsMap.get("javax.ejb.EJB");
        List<EJBReferenceInfo> listOfEJBRefs = null;
        EJBReferenceInfo ejbInfo = null;
        String ejbJNDIName = null;
        String businessType = null;
        String ejbName = null;
        ArrayList<Properties> ejbPropList = 
            (ArrayList<Properties>)ejbRefAnnotationMap.get(AnnotationsReader.FIELD_LIST);
        if (ejbPropList != null) {
            for (Properties ejbProp: ejbPropList) {
                listOfEJBRefs = new ArrayList<EJBReferenceInfo>();
                ejbInfo = new EJBReferenceInfo();
                listOfEJBRefs.add(ejbInfo);
                ejbJNDIName = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (ejbJNDIName == null) {
                    ejbJNDIName = 
                            (String)ejbProp.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
                }
                ejbInfo.setJNDIName(ejbJNDIName);

                ejbName = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_REFERENCE_BEANNAME);
                if (ejbName == null) {
                    ejbName = (String)ejbProp.get(AnnotationsReader.CLASSNAME);
                }
                ejbInfo.setEJBRefName(ejbName);

                businessType = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_REFERENCE_BEANINFO);
                if (businessType != null) {
                    ejbInfo.setEJBRefType(businessType);
                }
            }
        }
        mdbInfo.setEJBReferenceInfo(listOfEJBRefs);
        return mdbInfo;
    }

    void processActivationConfigAnnotation(org.netbeans.modules.classfile.ArrayElementValue ee, 
                                           Map activationCfgMap) {
        //org.netbeans.modules.classfile.ArrayElementValue aee = (org.netbeans.modules.classfile.ArrayElementValue)acc.getValue();
        org.netbeans.modules.classfile.ElementValue[] aelemArry = 
            ee.getValues();
        for (int ix = 0; ix < aelemArry.length; ix++) {
            NestedElementValue nv = (NestedElementValue)aelemArry[ix];
            org.netbeans.modules.classfile.Annotation nbAnn = 
                nv.getNestedValue();
            org.netbeans.modules.classfile.AnnotationComponent propNameAC = 
                nbAnn.getComponent("propertyName");
            org.netbeans.modules.classfile.AnnotationComponent propValueAC = 
                nbAnn.getComponent("propertyValue");

            Object propValueObj = propNameAC.getValue();
            String annKey = null;
            String annValue = null;
            if (propValueObj instanceof PrimitiveElementValue) {
                annKey = 
                        ((PrimitiveElementValue)propValueObj).getValue().getValue().toString();
            }
            propValueObj = propValueAC.getValue();
            if (propValueObj instanceof PrimitiveElementValue) {
                annValue = 
                        ((PrimitiveElementValue)propValueObj).getValue().getValue().toString();
            }
            activationCfgMap.put(annKey, annValue);
        }
    }


}


