/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations;


import org.glassfish.openesb.addons.appverifier.ErrorBundle;

import java.io.File;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import java.util.logging.Logger;

import org.netbeans.modules.classfile.Annotation;
import org.netbeans.modules.classfile.AnnotationComponent;
import org.netbeans.modules.classfile.ArrayElementValue;
import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;
import org.netbeans.modules.classfile.ElementValue;
import org.netbeans.modules.classfile.NestedElementValue;
import org.netbeans.modules.classfile.PrimitiveElementValue;

/**
 * Helper reads EJB annotations from class
 * @author Sreeni Genipudi
 */
public class AnnotationsReader {

    private static AnnotationsReader mInstance = null;
    public static final String CLASSNAME = "CLASSNAME";
    public static final String FIELD_LIST = "FIELD_LIST";
    private static Logger logger = 
        Logger.getLogger("com.stc.applicationVerifier.javaee.ejb.annotations.AnnotationsReader");

    private AnnotationsReader() {

    }

    public boolean contains(Class classObject, Class tagName) {
        if (classObject.getAnnotation(tagName) != null) {
            return true;
        }
        return false;
    }

    public boolean contains(ClassFile cf, 
                            ClassName annotationName) throws IOException {
        if (cf.getAnnotation(annotationName) != null) {
            return true;
        }
        return false;
    }

    public static AnnotationsReader getInstance() {
        if (mInstance == null) {
            mInstance = new AnnotationsReader();
        }
        return mInstance;
    }

    /**
     * @param classFile
     * @param annotationPropertyMap
     * @return
     */
    public String getValue(ClassFile cf, 
                           Map<String, Map<String, Object>> annotationPropertyMap, 
                           ErrorBundle err) throws IOException {
        Map<String, Map<String, Object>> propertyMap = annotationPropertyMap;
        String className = null;
        className = cf.getName().getExternalName().replace('/', '.');
        Set<Map.Entry<String, Map<String, Object>>> setPropertyMap = 
            propertyMap.entrySet();
        boolean bResources = false;
        for (Map.Entry<String, Map<String, Object>> mp: setPropertyMap) {
            String cl = mp.getKey();
            ClassName cln = ClassName.getClassName(cl.replace('.', '/'));
            Annotation ann = cf.getAnnotation(cln);
            bResources = cl.equals("javax.annotation.Resources");
            if (ann != null) {
                Map<String, Object> clAnnotationAttributeMap = mp.getValue();
                if (clAnnotationAttributeMap != null) {
                    Set<Map.Entry<String, Object>> annotationAttSet = 
                        clAnnotationAttributeMap.entrySet();
                    for (Map.Entry<String, Object> annAttEntry: 
                         annotationAttSet) {
                        String componentName = annAttEntry.getKey();

                        AnnotationComponent annComp = 
                            ann.getComponent(componentName);
                        if (annComp != null || bResources) {
                            Object annValueObj = null;
                            if (bResources) {
                                annValueObj = 
                                        ann.getComponents()[0].getValue();
                            } else {
                                annValueObj = annComp.getValue();
                            }
                            // org.netbeans.modules.classfile.PrimitiveElementValue!!!!!
                            String annVaue = null;
                            if (annValueObj instanceof PrimitiveElementValue) {
                                annVaue = 
                                        ((PrimitiveElementValue)annValueObj).getValue().getValue().toString();
                            } else if (annValueObj instanceof 
                                       ArrayElementValue) {
                                if (bResources) {
                                    ElementValue[] elemArrayValue = 
                                        ((ArrayElementValue)annValueObj).getValues();
                                    List<Map<String, Object>> listOfReferences = 
                                        (List<Map<String, Object>>)annAttEntry.getValue();

                                    Map<String, Object> mapAnnotationkeyValue = 
                                        annotationPropertyMap.get(componentName);
                                    Set mapAnnotationKeySet = 
                                        mapAnnotationkeyValue.keySet();
                                    for (ElementValue elemValue: 
                                         elemArrayValue) {
                                        Map<String, Object> newReferenceInArray = 
                                            new HashMap<String, Object>();
                                        Iterator<String> newRefItr = 
                                            mapAnnotationKeySet.iterator();
                                        if (elemValue instanceof 
                                            org.netbeans.modules.classfile.NestedElementValue) {
                                            Annotation annElem = 
                                                ((org.netbeans.modules.classfile.NestedElementValue)elemValue).getNestedValue();
                                            while (newRefItr.hasNext()) {
                                                String refKey = 
                                                    newRefItr.next();
                                                AnnotationComponent annElemComp = 
                                                    annElem.getComponent(refKey);
                                                Object annCompValueObj = 
                                                    annElemComp.getValue();
                                                String annCompValue = null;
                                                if (annCompValueObj instanceof 
                                                    PrimitiveElementValue) {
                                                    annCompValue = 
                                                            ((PrimitiveElementValue)annCompValueObj).getValue().getValue().toString();
                                                } else {
                                                    annCompValue = 
                                                            annCompValueObj.toString();
                                                }
                                                newReferenceInArray.put(refKey, 
                                                                        annCompValue);
                                            }
                                            if (listOfReferences == null) {
                                                listOfReferences = 
                                                        new ArrayList<Map<String, Object>>();
                                                clAnnotationAttributeMap.put(componentName, 
                                                                             listOfReferences);
                                            }
                                            listOfReferences.add(newReferenceInArray);
                                        }
                                    }
                                } else {
                                    annVaue = annValueObj.toString();
                                }
                            } else {

                                annVaue = annValueObj.toString();
                            }
                            if (!bResources) {
                                clAnnotationAttributeMap.put(componentName, 
                                                             annVaue);
                            }
                        }

                    }
                }
            } else {
                //Check fields

                Map<String, Object> fieldMapRef = propertyMap.get(cl);
                Collection<org.netbeans.modules.classfile.Variable> colVar = 
                    cf.getVariables();
                for (org.netbeans.modules.classfile.Variable var: colVar) {
                    Annotation varAnn = var.getAnnotation(cln);
                    if (varAnn != null) {
                        Map<String, Object> clAnnotationAttributeMap = 
                            mp.getValue();
                        Set<Map.Entry<String, Object>> annotationAttSet = 
                            clAnnotationAttributeMap.entrySet();
                        //                         ArrayList<Properties> listOfResources = new ArrayList<Properties>();
                        ArrayList<Properties> listOfResources = 
                            (ArrayList<Properties>)fieldMapRef.get(FIELD_LIST);
                        if (listOfResources == null) {
                            listOfResources = new ArrayList<Properties>();
                            fieldMapRef.put(FIELD_LIST, listOfResources);
                        }
                        Properties resRefProperties = new Properties();
                        listOfResources.add(resRefProperties);
                        //Set resource type 
                        resRefProperties.setProperty(CLASSNAME, 
                                                     ClassName.getClassName(var.getDescriptor()).getExternalName());

                        String jndiName = null;
                        for (Map.Entry<String, Object> annAttEntry: 
                             annotationAttSet) {
                            String componentName = annAttEntry.getKey();
                            AnnotationComponent annComp = 
                                varAnn.getComponent(componentName);

                            if (annComp != null) {
                                Object resRefPropValueObj = annComp.getValue();
                                String resRefPropValue = null;
                                if (resRefPropValueObj != null) {
                                    if (resRefPropValueObj instanceof 
                                        PrimitiveElementValue) {
                                        resRefPropValue = 
                                                ((PrimitiveElementValue)resRefPropValueObj).getValue().getValue().toString();

                                    } else {
                                        resRefPropValue = 
                                                resRefPropValueObj.toString();
                                    }
                                    resRefProperties.setProperty(componentName, 
                                                                 resRefPropValue);
                                }

                            }
                        }
                    }
                }

            }
        }
        return className;
    }

    /**
     * 
     * @param ejbClassName
     * @param excludeList
     * @param tagName like  
     * @return
     */
    public ClassName getInterface(ClassFile ejbCF, List<String> excludeList, 
                                  Class tagName, ErrorBundle eb, 
                                  ClassLoader cl) {
        java.util.Collection<org.netbeans.modules.classfile.ClassName> colCN = 
            ejbCF.getInterfaces();
        int excludedIntfIndx = 0;
        List<ClassName> listOfLeftOvers = new ArrayList<ClassName>();
        for (org.netbeans.modules.classfile.ClassName intClassName: colCN) {
            String interfaceName = intClassName.getExternalName();
            if (excludeList != null && excludeList.contains(interfaceName)) {
                excludedIntfIndx++;
                continue;
            } else {
                listOfLeftOvers.add(intClassName);
                // Need to check if the class contains 
                // tag .. this.contains(intClassName, tagName) 
                // avoiding for now to avoid expanding EAR file.
                /*                if (tagName != null ){
                    return interfaceArry[in];
                }*/
            }
        }

        if (colCN.size() == 1 && excludedIntfIndx == 0) {
            return colCN.iterator().next();
        }

        int leftOverIntfNumber = colCN.size() - excludedIntfIndx;
        if (listOfLeftOvers.size() > 0) {
            return listOfLeftOvers.get(0);
        } /* else {

            if (leftOverIntfNumber > 1) {
                eb.addError(" More than one interface found in "+ejbClassName.getExternalName());
            } else {
                eb.addInfo(" No interface found in "+ejbClassName.getExternalName());
            }
        }*/
        return null;
    }

    public void processAnnotations(Annotation ann, Map annotationMap) {
        org.netbeans.modules.classfile.AnnotationComponent[] ac = 
            ann.getComponents();
        for (int ind = 0; ind < ac.length; ind++) {
            AnnotationComponent acc = ac[ind];
            logger.fine(" Annotation Component Name " + acc.getName());
            logger.fine(" Annotation Component value " + acc.getValue());
            logger.fine(" Annotation Component value class " + 
                        acc.getValue().getClass().getName());
            annotationMap.put(acc.getName(), acc.getValue());
        }
    }

    public String getPrimitiveValue(Object propValueObj) {
        if (propValueObj != null) {
            if (propValueObj instanceof PrimitiveElementValue) {
                return ((PrimitiveElementValue)propValueObj).getValue().getValue().toString();
            } else {
                return propValueObj.toString();
            }

        }
        return null;
    }


}
