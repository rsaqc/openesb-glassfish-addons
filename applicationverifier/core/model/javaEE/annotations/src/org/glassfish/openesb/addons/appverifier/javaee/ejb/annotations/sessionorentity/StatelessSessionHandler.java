/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.sessionorentity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.glassfish.openesb.addons.appverifier.ErrorBundle;
import org.glassfish.openesb.addons.appverifier.OutboundRARValidator;
import org.glassfish.openesb.addons.appverifier.RAHelper;
import org.glassfish.openesb.addons.appverifier.ValidationConfig;
import org.glassfish.openesb.addons.appverifier.VerifyStatus;
import org.glassfish.openesb.addons.appverifier.ejb.EJBInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EJBReferenceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.EjbJavaEEXMLReader;
import org.glassfish.openesb.addons.appverifier.ejb.ResourceInfo;
import org.glassfish.openesb.addons.appverifier.ejb.SessionBeanInfo;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.AnnotationsReader;
import org.glassfish.openesb.addons.appverifier.javaee.ejb.annotations.EJBAnnotationsHandler;
import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;
import org.glassfish.openesb.addons.appverifier.javaee.sunejb.descriptor.SunEjbJar;

import org.netbeans.modules.classfile.ClassFile;
import org.netbeans.modules.classfile.ClassName;

/**
 * Stateless session bean handler
 * @author Sreeni Genipudi
 */
public class StatelessSessionHandler implements EJBAnnotationsHandler {
    private final ArrayList<String> mExcludeClasses = new ArrayList<String>();
    private ResourceBundle mRb = 
        ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.ejb.Bundle", 
                                 Locale.getDefault());

    public StatelessSessionHandler() {
    }

    public Map<String, Map<String, Object>> createMap() {

        Map<String, Map<String, Object>> ssbAnnotationsMap = 
            new HashMap<String, Map<String, Object>>();

        //1. EJB MAP
        Map<String, Object> ejbAttrMap = new HashMap<String, Object>();
        ejbAttrMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        ejbAttrMap.put(EJBAnnotationsHandler.EJB_MAPPED_NAME, null);
        ssbAnnotationsMap.put(getType(), ejbAttrMap);

        // 2. Resource Reference
        Map<String, Object> resRefMap = new HashMap<String, Object>();
        resRefMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        resRefMap.put(EJBAnnotationsHandler.RESOURCE_MAPPED_NAME, null);
        resRefMap.put(EJBAnnotationsHandler.RESOURCE_ANN_TYPE, Class.class);
        ssbAnnotationsMap.put("javax.annotation.Resource", resRefMap);

        //3. Resources Reference
        Map<String, Object> resourcesRefMap = new HashMap<String, Object>();
        List<Map<String, Object>> listOfResources = 
            new ArrayList<Map<String, Object>>();

        resourcesRefMap.put("javax.annotation.Resource", listOfResources);
        ssbAnnotationsMap.put("javax.annotation.Resources", resourcesRefMap);


        // 4. EJB References
        Map<String, Object> ejbRefMap = new HashMap<String, Object>();
        ejbRefMap.put(EJBAnnotationsHandler.EJB_NAME, null);
        ejbRefMap.put(EJBAnnotationsHandler.RESOURCE_MAPPED_NAME, null);
        ejbRefMap.put(EJBAnnotationsHandler.RESOURCE_MAPPED_NAME, null);
        ejbRefMap.put(EJBAnnotationsHandler.EJB_REFERENCE_BEANINFO, 
                      Object.class);
        ejbRefMap.put(EJBAnnotationsHandler.EJB_REFERENCE_BEANNAME, 
                      Object.class);
        ssbAnnotationsMap.put("javax.ejb.EJB", ejbRefMap);


        mExcludeClasses.add("java.io.Serializable");
        mExcludeClasses.add("java.io.Externalizable");
        mExcludeClasses.add("javax.ejb.EJBContext");
        mExcludeClasses.add("javax.ejb.EJBHome");
        mExcludeClasses.add("javax.ejb.EJBLocalHome");
        mExcludeClasses.add("javax.ejb.EJBLocalObject");
        mExcludeClasses.add("javax.ejb.EJBMetaData");
        mExcludeClasses.add("javax.ejb.EJBObject");
        mExcludeClasses.add("javax.ejb.EnterpriseBean");
        mExcludeClasses.add("javax.ejb.EntityBean");
        mExcludeClasses.add("javax.ejb.EntityContext");
        mExcludeClasses.add("javax.ejb.Handle");
        mExcludeClasses.add("javax.ejb.HomeHandle");
        mExcludeClasses.add("javax.ejb.MessageDrivenBean");
        mExcludeClasses.add("javax.ejb.MessageDrivenContext");
        mExcludeClasses.add("javax.ejb.SessionBean");
        mExcludeClasses.add("javax.ejb.SessionContext");
        mExcludeClasses.add("javax.ejb.SessionSynchronization");
        mExcludeClasses.add("javax.ejb.TimedObject");
        mExcludeClasses.add("javax.ejb.Timer");
        mExcludeClasses.add("javax.ejb.TimerHandle");
        mExcludeClasses.add("javax.ejb.TimerService");
        return ssbAnnotationsMap;
    }

    public EJBInfo process(Map<String, Map<String, Object>> annotationsMap, 
                           List<InboundResourceAdapter> inbound, 
                           List<OutboundResourceAdapter> out, 
                           SunEjbJar sunEjbJarDesc, ValidationConfig vc, 
                           ClassLoader loader, String className, 
                           ClassFile classFile, ErrorBundle errorBundle, 
                           String instance) {
        //process annotations.
        SessionBeanInfo ssbInfo = new SessionBeanInfo(vc, loader, errorBundle);
        // className = className.replace('.','/');
        Map<String, Object> ssbAnnotationMap = annotationsMap.get(getType());
        AnnotationsReader annRdr = AnnotationsReader.getInstance();
        Object propValueObj = 
            ssbAnnotationMap.get(EJBAnnotationsHandler.EJB_NAME);
        String ssbName = annRdr.getPrimitiveValue(propValueObj);

        propValueObj = 
                ssbAnnotationMap.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
        String ssbJNDIName = annRdr.getPrimitiveValue(propValueObj);


        propValueObj = 
                ssbAnnotationMap.get(EJBAnnotationsHandler.EJB_CLASS_NAME);
        String ssbClassName = annRdr.getPrimitiveValue(propValueObj);
        if (ssbName == null) {
            if (ssbClassName == null) {
                ssbClassName = className;
                ssbClassName.replace('.', '/');
                int nameIndx = ssbClassName.lastIndexOf("/");
                if (nameIndx > 0) {
                    ssbClassName = className.substring(nameIndx + 1);
                }
            }
            ssbName = ssbClassName;
        }

        ssbInfo.setEJBName(ssbName);
        if (ssbJNDIName != null) {
            ssbInfo.setJNDIName(ssbJNDIName);
        }
        ssbInfo.setEJBClassName(className.replace('/', '.'));
        ClassName buisnessType = null;
        AnnotationsReader annReader = null;
        Class tagName = javax.ejb.Local.class;
        annReader = AnnotationsReader.getInstance();
        //Class mdbClass = Class.forName(className);
        ClassName mdbCN = ClassName.getClassName(className.replace('.', '/'));
        buisnessType = 
                annReader.getInterface(classFile, mExcludeClasses, tagName, 
                                       errorBundle, loader);

        if (buisnessType != null) {
            ssbInfo.setLocal(buisnessType.getExternalName());
        }
        tagName = javax.ejb.Remote.class;
        annReader = AnnotationsReader.getInstance();
        //Class mdbClass = Class.forName(className);
        mdbCN = ClassName.getClassName(className.replace('.', '/'));
        buisnessType = 
                annReader.getInterface(classFile, mExcludeClasses, tagName, 
                                       errorBundle, loader);
        if (buisnessType != null) {
            ssbInfo.setRemote(buisnessType.getInternalName());
        }

        //Process Resource


        Map<String, Object> resRefAnnotationMap = 
            annotationsMap.get("javax.annotation.Resource");
        OutboundRARValidator outRARValidator = 
            OutboundRARValidator.getInstance();
        RAHelper raHlpr = RAHelper.getInstance(vc, errorBundle, instance);


        List<ResourceInfo> listOfResources = null;
        ResourceInfo resInfo = null;
        String resourceJNDIName = null;
        String resourceType = null;
        String resourceName = null;
        String resDescr = null;
        listOfResources = new ArrayList<ResourceInfo>();

        Map<String, Object> resourcesRefAnnotationMap = 
            annotationsMap.get("javax.annotation.Resources");
        List<Map<String, Object>> listOfResourceArry = 
            (List<Map<String, Object>>)resourcesRefAnnotationMap.get("javax.annotation.Resource");

        if (listOfResourceArry != null) {
            for (Map<String, Object> resourceData: listOfResourceArry) {

                String resName = 
                    (String)resourceData.get(EJBAnnotationsHandler.EJB_NAME);
                String resMapName = null;


                if (resName != null) {
                    resInfo = new ResourceInfo();
                    listOfResources.add(resInfo);
                    resInfo.setResourceJNDIName(resName);

                    resMapName = 
                            (String)resourceData.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
                    if (resMapName == null) {
                        resInfo.setResourceRefName(resName);
                    }

                    resourceType = 
                            (String)resourceData.get(EJBAnnotationsHandler.RESOURCE_ANN_TYPE);
                    if (resourceType != null) {
                        resInfo.setResourceType(resourceType);
                    }


                    resDescr = 
                            (String)resourceData.get(EJBAnnotationsHandler.RESOURCE_DESCRIPTION);
                    resDescr = (resDescr != null) ? resDescr : "";

                    if (outRARValidator.isResourceLocal(resMapName)) {
                        if (!outRARValidator.isValidLocalResource(resMapName, 
                                                                  resourceType, 
                                                                  out, 
                                                                  errorBundle)) {
                            resInfo.setStatus(VerifyStatus.ERROR);
                            resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_LOCAL_RESOURCE) + 
                                               " " + resMapName + " " + 
                                               mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                               resourceType + " " + resDescr);
                        }
                    } else {
                        if (!raHlpr.isGlobalResource(resMapName, resourceType, 
                                                     errorBundle)) {
                            resInfo.setStatus(VerifyStatus.ERROR);
                            resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                                               " " + resMapName + " " + 
                                               mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                               resourceType + " " + resDescr);

                        }
                    }

                }

            }
        }


        String resName = 
            (String)resRefAnnotationMap.get(EJBAnnotationsHandler.EJB_NAME);
        String resMapName = null;


        if (resName != null) {
            resInfo = new ResourceInfo();
            listOfResources.add(resInfo);
            resInfo.setResourceJNDIName(resName);

            resMapName = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
            if (resMapName == null) {
                resInfo.setResourceRefName(resName);
            }

            resourceType = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.RESOURCE_ANN_TYPE);
            if (resourceType != null) {
                resInfo.setResourceType(resourceType);
            }


            resDescr = 
                    (String)resRefAnnotationMap.get(EJBAnnotationsHandler.RESOURCE_DESCRIPTION);
            resDescr = (resDescr != null) ? resDescr : "";

            if (outRARValidator.isResourceLocal(resMapName)) {
                if (!outRARValidator.isValidLocalResource(resMapName, 
                                                          resourceType, out, 
                                                          errorBundle)) {
                    resInfo.setStatus(VerifyStatus.ERROR);
                    resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_LOCAL_RESOURCE) + 
                                       " " + resMapName + " " + 
                                       mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                       resourceType + " " + resDescr);
                }
            } else {
                if (!raHlpr.isGlobalResource(resMapName, resourceType, 
                                             errorBundle)) {
                    resInfo.setStatus(VerifyStatus.ERROR);
                    resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                                       " " + resMapName + " " + 
                                       mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                       resourceType + " " + resDescr);

                }
            }

        }

        ArrayList<Properties> resProp = 
            (ArrayList<Properties>)resRefAnnotationMap.get(AnnotationsReader.FIELD_LIST);

        if (resProp != null) {
            for (Properties resourceProp: resProp) {

                resInfo = new ResourceInfo();

                resourceJNDIName = 
                        (String)resourceProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (resourceJNDIName == null) {
                    resourceJNDIName = 
                            (String)resourceProp.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);
                }

                resourceName = 
                        (String)resourceProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (resourceName == null) {
                    resourceName = resourceJNDIName;
                }
                if (resourceJNDIName == null) {
                    resourceJNDIName = resourceName;
                }
                if (resourceJNDIName != null) {
                    listOfResources.add(resInfo);
                    resInfo.setResourceJNDIName(resourceJNDIName);
                    resInfo.setResourceRefName(resourceName);

                    resourceType = 
                            (String)resourceProp.get(EJBAnnotationsHandler.RESOURCE_ANN_TYPE);
                    if (resourceType == null) {
                        resourceType = 
                                (String)resourceProp.get(AnnotationsReader.CLASSNAME);
                    }
                    resInfo.setResourceType(resourceType);
                    resDescr = 
                            (String)resourceProp.get(EJBAnnotationsHandler.RESOURCE_DESCRIPTION);
                    resDescr = (resDescr != null) ? resDescr : "";

                    if (outRARValidator.isResourceLocal(resourceJNDIName)) {
                        if (!outRARValidator.isValidLocalResource(resourceJNDIName, 
                                                                  resourceType, 
                                                                  out, 
                                                                  errorBundle)) {
                            resInfo.setStatus(VerifyStatus.ERROR);
                            resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_LOCAL_RESOURCE) + 
                                               " " + resourceJNDIName + " " + 
                                               mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                               resourceType + " " + resDescr);
                        }
                    } else {
                        if (!raHlpr.isGlobalResource(resourceJNDIName, 
                                                     resourceType, 
                                                     errorBundle)) {
                            resInfo.setStatus(VerifyStatus.ERROR);
                            resInfo.setMessage(mRb.getString(EjbJavaEEXMLReader.CANNOT_RESOLVE_GLOBAL_RESOURCE) + 
                                               " " + resourceJNDIName + " " + 
                                               mRb.getString(EjbJavaEEXMLReader.RESOURCE_TYPE) + 
                                               resourceType + " " + resDescr);
                        }
                    }
                }

            }
        }
        ssbInfo.setResourceReferenceInfo(listOfResources);

        //EJBREFS
        Map<String, Object> ejbRefAnnotationMap = 
            annotationsMap.get("javax.ejb.EJB");
        List<EJBReferenceInfo> listOfEJBRefs = null;
        EJBReferenceInfo ejbInfo = null;
        String ejbJNDIName = null;
        String businessType = null;
        String ejbName = null;
        ArrayList<Properties> ejbPropList = 
            (ArrayList<Properties>)ejbRefAnnotationMap.get(AnnotationsReader.FIELD_LIST);
        if (ejbPropList != null) {
            for (Properties ejbProp: ejbPropList) {
                listOfEJBRefs = new ArrayList<EJBReferenceInfo>();
                ejbInfo = new EJBReferenceInfo();
                listOfEJBRefs.add(ejbInfo);
                ejbJNDIName = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_NAME);
                if (ejbJNDIName == null) {
                    ejbJNDIName = 
                            (String)ejbProp.get(EJBAnnotationsHandler.EJB_MAPPED_NAME);

                }
                ejbInfo.setJNDIName(ejbJNDIName);

                ejbName = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_REFERENCE_BEANNAME);
                if (ejbName == null) {
                    ejbName = (String)ejbProp.get(AnnotationsReader.CLASSNAME);
                }
                ejbInfo.setEJBRefName(ejbName);

                businessType = 
                        (String)ejbProp.get(EJBAnnotationsHandler.EJB_REFERENCE_BEANINFO);
                if (businessType != null) {
                    ejbInfo.setEJBRefType(businessType);
                }
            }
        }
        ssbInfo.setEJBReferenceInfo(listOfEJBRefs);

        return ssbInfo;
    }

    public String getType() {
        return "javax.ejb.Stateless";
    }
}
