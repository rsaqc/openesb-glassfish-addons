@echo off
set APPVERIFIER_DIR=.
set APPVERIFIER_LIB=%APPVERIFIER_DIR%/lib
set CLASSPATH=%APPVERIFIER_DIR%/config;%APPVERIFIER_LIB%/activation.jar;%APPVERIFIER_LIB%/ant-launcher.jar;%APPVERIFIER_LIB%/ant.jar;%APPVERIFIER_LIB%/appserv-jstl.jar;%APPVERIFIER_LIB%/com-sun-commons-logging.jar;%APPVERIFIER_LIB%/com.sun.soabi.capsverifier.jar;%APPVERIFIER_LIB%/javaee.jar;%APPVERIFIER_LIB%/jaxb-impl.jar;%APPVERIFIER_LIB%/mail.jar;%APPVERIFIER_LIB%/org-netbeans-modules-classfile.jar;%APPVERIFIER_LIB%/webservices-rt.jar;%APPVERIFIER_LIB%/webservices-tools.jar
java com.sun.soabi.capsverifier.ApplicationVerifierMain %1