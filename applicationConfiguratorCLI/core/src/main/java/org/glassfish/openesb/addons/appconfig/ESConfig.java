/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ESConfig.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.util.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;

/**
 *
 * @author Sun Microsystems
 * 
 * External System Configuration.
 */
public class ESConfig {

    public static final String B64_PREFIX = Constants.B64_PREFIX;
    public static final String B64_GZ_PREFIX = Constants.B64_GZ_PREFIX;
    public static final String CDATA_PREFIX = Constants.CDATA_PREFIX;
    public static final String CDATA_SUFFIX = Constants.CDATA_SUFFIX;
    public final static String CDATA_START_ENTITY_REF = Constants.CDATA_START_ENTITY_REF;
    public final static String CDATA_END_ENTITY_REF = Constants.CDATA_END_ENTITY_REF;
    public static final String KEY_SEPARATOR = Constants.KEY_SEPARATOR;
    // Configuration template(+instance) DOM. While eWays configuration DOM 
    // has both <template> and <instance> parts, JMS configuration does not have
    // <instance>. For JMS values of the template params are obtained from the
    // XML descriptor (rar.xml).
    private Document docConfig;
    // XML Descriptor that contains this Configuration property
    private Document docParentDesc;
    private Set<String> cmapParams;
    private XPathFactory xpf = XPathFactory.newInstance();
    private XPath xp = xpf.newXPath();

    public ESConfig(String xml) throws Exception {
        this(xml, null, null);
    }

    public ESConfig(String xml, String strCmapParams, Document parent) throws Exception {
        this(DOMUtil.parse(xml), parent, strCmapParams);
    }
    
    public ESConfig(Document docCfg, Document docParent, String strCmapParams) throws Exception {
        docConfig = docCfg;
        docParentDesc = docParent;
        cmapParams = new HashSet<String>();
        if (strCmapParams != null) {
            StringTokenizer st = new StringTokenizer(strCmapParams, ",");
            while (st.hasMoreTokens()) {
                cmapParams.add(st.nextToken());
            }
        }
    }

    public Map<String, Node> getDescriptorParamNodes(Map<String, Node> templParams) throws Exception {

        Map<String, Node> map = new LinkedHashMap<String, Node>();

        Set<String> keys = templParams.keySet();
        for (Iterator<String> it = keys.iterator(); it.hasNext();) {
            String paramKey = it.next();
            String paramName = paramKey.substring(
                    paramKey.lastIndexOf(Constants.KEY_SEPARATOR) + 1);
            String propXP = Constants.RAR_PROP_XP.replace("?", paramName);
            Node configprop = (Node) xp.evaluate(propXP, docParentDesc, XPathConstants.NODE);
            if (configprop != null) {
                map.put(paramKey, configprop);
            }
        }

        return map;
    }

    public boolean hasInstance() throws Exception {
        Node instance = (Node) xp.evaluate("//instance", docConfig,
                XPathConstants.NODE);

        if (instance != null) {
            return true;
        }

        return false;
    }

    public Map<String, Node> getInstanceParamNodes() throws Exception {

        if (!hasInstance()) {
            return null;
        }

        Node instance = (Node) xp.evaluate("//instance", docConfig,
                XPathConstants.NODE);

        Map<String, Node> map = new LinkedHashMap<String, Node>();
        NodeList isections = (NodeList) xp.evaluate(
                "cfg/configuration/section", instance,
                XPathConstants.NODESET);

        String parentSectionName = null;
        for (int i = 0; i < isections.getLength(); i++) {
            Node section = (Node) isections.item(i);
            addSectionParams(section, parentSectionName, map);
        }

        return map;
    }

    public Node getTemplate() throws Exception {

        Node template = (Node) xp.evaluate("//template", docConfig,
                XPathConstants.NODE);

        if(template == null){
            // Unified JMS JCA config template does not have a <template> node.
            template = docConfig;
        }
        
        return template;
    }
    
    public Map<String, Node> getTemplateParamNodes() throws Exception {
        
        Node template = getTemplate();
        
        Map<String, Node> map = new LinkedHashMap<String, Node>();
        NodeList tsections = (NodeList) xp.evaluate(
                "cfg/configuration/section", template,
                XPathConstants.NODESET);

        String parentSectionName = null;
        for (int i = 0; i < tsections.getLength(); i++) {
            Node section = (Node) tsections.item(i);
            addSectionParams(section, parentSectionName, map);
        }

        return map;
    }

    public Document getConfigDOM() {
        return docConfig;
    }

    public List<CAPSProperty> getEnvParamProperties() throws Exception {
        Map<String, Node> tParams = getTemplateParamNodes();
        Map<String, Node> iParams = getInstanceParamNodes();
        if (iParams == null) {
            iParams = getDescriptorParamNodes(tParams);
        }

        List<CAPSProperty> props = getParamProperties(tParams, iParams, true);
        return props;
    }

    public List<CAPSProperty> getCMParamProperties() throws Exception {
        Map<String, Node> tParams = getTemplateParamNodes();
        Map<String, Node> iParams = getInstanceParamNodes();
        if (iParams == null) {
            iParams = getDescriptorParamNodes(tParams);
        }

        List<CAPSProperty> props = getParamProperties(tParams, iParams, false);
        return props;
    }

    private List<CAPSProperty> getParamProperties(Map<String, Node> tParams,
            Map<String, Node> iParams, boolean env) throws Exception {

        List<CAPSProperty> props = new ArrayList<CAPSProperty>();

        boolean useLongKeys = ConfigUtil.checkNameConflicts(tParams);

        Set<String> iparamKeys = iParams.keySet();
        for (Iterator<String> it = iparamKeys.iterator(); it.hasNext();) {

            String paramKey = it.next();

            Node tparam = tParams.get(paramKey);
            Node iparam = iParams.get(paramKey);

            NamedNodeMap tAttribs = tparam.getAttributes();
            if (!ConfigUtil.isEditable(tAttribs)) {
                continue;
            }

            String name = tAttribs.getNamedItem("name").getTextContent();

            if (env) {
                if (cmapParams.contains(name)) {
                    // exclude CMap params
                    continue;
                }
            } else if (!cmapParams.contains(name)) {
                // exclude Env param
                continue;
            }

            String key = name;
            if (useLongKeys) {
                key = paramKey;
            }

            String value = "";
            Node valueNode = (Node) xp.evaluate("value", iparam, XPathConstants.NODE);
            if (valueNode == null) {
                valueNode = (Node) xp.evaluate("config-property-value", iparam, XPathConstants.NODE);
            } if (valueNode == null) {
                valueNode = (Node) xp.evaluate("default/value", iparam, XPathConstants.NODE);
            }

            if (valueNode != null) {
                value = valueNode.getTextContent();
                if(key.equals("Options")){
                    //print("Options: " + value);
                }
            }

            CAPSProperty prop = getProperty(key, value, tparam);
            props.add(prop);
        }

        return props;

    }

    private CAPSProperty getProperty(String name, String value, Node tparam) throws Exception {

        NamedNodeMap tAttribs = tparam.getAttributes();

        String dispName = tAttribs.getNamedItem("displayName").getTextContent();

        // get description
        Node nodeDescription = (Node) xp.evaluate("description",
                tparam, XPathConstants.NODE);
        String description = "";
        if (nodeDescription != null) {
            description = nodeDescription.getTextContent();
        }

        // choiceList
        NodeList nlChoiceList = (NodeList) xp.evaluate(
                "./choiceList/value", tparam,
                XPathConstants.NODESET);

        List<String> choiceList = new ArrayList<String>();
        if (nlChoiceList != null) {
            int len = nlChoiceList.getLength();
            for (int i = 0; i < len; i++) {
                Node choice = nlChoiceList.item(i);
                choiceList.add(choice.getTextContent());
            }
        }

        // isEncrypted
        Node encrypAttrib = tAttribs.getNamedItem("isEncrypted");
        boolean isEncrypted = false;
        if (encrypAttrib != null) {
            isEncrypted = Boolean.parseBoolean(encrypAttrib.getTextContent());
        }

        description = clean(description);

        value = clean(value);

        if ( isEncrypted && (!"<password>".equals(value)) 
                && !value.startsWith(Constants.ENC07) 
                && !value.startsWith(Constants.ENC08) ) {
            // JMS encrypted values are always prefixed, so if the value has 
            // no prefix, assume its an eWay, and use the eWay encryption prefix
            value = Constants.ENC07 + value;
        }

        return new CAPSProperty(name, dispName, value, description, choiceList);
    }

    private String clean(String value) {

        if (value != null && value.startsWith(Constants.CDATA_PREFIX)) {
            value = value.substring(Constants.CDATA_PREFIX.length(),
                    value.length() - Constants.CDATA_SUFFIX.length());
        }

        return value;
    }

    private void addSectionParams(Node section, String parentSectionKey, Map<String, Node> map) throws Exception {

        NamedNodeMap sectionAttribs = section.getAttributes();
        String sectionKey = sectionAttribs.getNamedItem("name").getTextContent();
        if (parentSectionKey != null) {
            sectionKey = parentSectionKey + KEY_SEPARATOR + sectionKey;
        }

        NodeList params = (NodeList) xp.evaluate("parameter", section, XPathConstants.NODESET);
        for (int i = 0; i < params.getLength(); i++) {
            Node param = params.item(i);
            NamedNodeMap attribs = param.getAttributes();
            String paramKey = sectionKey + KEY_SEPARATOR + attribs.getNamedItem("name").getTextContent();
            //Node valueNode = (Node) xp.evaluate("value", param, XPathConstants.NODE);
            map.put(paramKey, param);
        }

        // add sub section params
        NodeList subSections = (NodeList) xp.evaluate("section",
                section, XPathConstants.NODESET);
        for (int j = 0; j < subSections.getLength(); j++) {
            Node subSection = subSections.item(j);
            addSectionParams(subSection, sectionKey, map);
        }

    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
