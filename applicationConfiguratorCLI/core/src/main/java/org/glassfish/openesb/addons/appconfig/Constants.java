/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import java.io.File;

/**
 *
 * @author Sun Microsystems
 */
public class Constants {

    public static final String MODULE_JAR = "JAR";
    public static final String MODULE_RAR = "RAR";
    public static final String MODULE_WAR = "WAR";
    
    public static final String B64_PREFIX = "BASE64";
    public static final String B64_GZ_PREFIX = "B64-GZ:";
    
    public static final String CDATA_PREFIX = "<![CDATA[";
    public static final String CDATA_SUFFIX = "]]>";
    
    public final static String CDATA_START_ENTITY_REF = "&lt;!\\[CDATA\\[";
    public final static String CDATA_END_ENTITY_REF = "]]&gt;";
    
    public static final String KEY_SEPARATOR = ".";
    
    public static final String ExtSysId_KEY = "extsys.id";
    public static final String ExtSysId_DISPLAY_NAME = "";
    public static final String ExtSysId_DESC = "External System Id: Do not modify.";
    
    public static final String Configuration = "Configuration";
    public static final String Configuration_DESC = "Configuration: Do not modify.";
    public static final String ConfigurationTemplate = "ConfigurationTemplate";
    
    public static final String UTF_8 = "UTF-8";
    
    public static final String ENC07 = "__:ENC:07:";
    public static final String ENC08 = "__:ENC:08:";
    
    public static final String JCA_PREFIX = "JCA_";
    public static final String LDAP = "LDAP";
    public static final String EINSIGHT_ENGINE = "eInsight_engine";
    
    public static final String CAPSENV_ROOT = "capsenv";
    
    public static final String ID_SEPARATOR = ":";
    public static final String FILE_PATH_SEPARATOR = File.separator;
    public static final String JNDI_PATH_SEPARATOR = "/";
    
    public static final String CM = "CM";
    public static final String ENV = "ENV";
    
    public static final String RAR_CONFIG_XP = "//config-property-value" +
            "[../config-property-name='" + "Configuration" + "']";
    public static final String JAR_CONFIG_XP = "//activation-config-property-value" +
            "[../activation-config-property-name='" + "Configuration" + "']";
    public static final String JMS_CONFIG_TEMPLATE_XP = "//config-property-value" +
            "[../config-property-name='" + "ConfigurationTemplate" + "']";
    
    public static final String RAR_PROP_XP = "/connector/resourceadapter/" +
            "config-property" + "[./config-property-name='" + "?" + "']";
    
    public static final String RAR_PROP_VALUE_XP = "/connector/resourceadapter/" +
            "config-property/config-property-value" + "[../config-property-name='" + "?" + "']";
    
    public static final String SUN_JMS_ADAPTER_NAME = "sun-jms-adapter";
    
    public static final String LINE_FEED = "\r\n";
    
    public static final String JMSJCA_MARK = "JMSJCA.sep=";
    
    public static final String JMS_CONNECTIONURL = "ConnectionURL";
    public static final String JMS_USERNAME = "UserName";
    public static final String JMS_PASSWORD = "Password";
    public static final String JMS_OPTIONS = "Options";
    
}
