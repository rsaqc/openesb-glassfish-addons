/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Ear.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 */
/**
 * @author Sun Microsystems
 *
 */
public class Ear {

    protected final static String MODULE_JAR = "JAR";
    protected final static String MODULE_RAR = "RAR";
    protected final static String MODULE_WAR = "WAR";
    private ZipFile zf = null;
    private String filepath = null;

    public Ear(String earPath) {
        try {
            filepath = earPath;
            zf = new ZipFile(filepath);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public String getPath() {
        return filepath;
    }

    public List<Node> listModules() throws IOException {
        ZipEntry zAppXml = zf.getEntry("META-INF/application.xml");
        if(zAppXml == null){
            throw new IOException("Invalid EAR: missing META-INF/application.xml." );
        }
        
        InputStream is = zf.getInputStream(zAppXml);
        //print("Modules: ");
        //return getModules(is);
        ArrayList<Node> l = new ArrayList<Node>();
        try {
            Document docApp = DOMUtil.parse(is);
            // get all module elements
            NodeList nlEjb = docApp.getElementsByTagName("ejb");
            NodeList nlConn = docApp.getElementsByTagName("connector");
            // currently we do not process web modules
            //NodeList nlWeb = docApp.getElementsByTagName("web");

            NodeList[] arrMods = new NodeList[]{nlEjb, nlConn};
            for (int i = 0; i < arrMods.length; i++) {
                NodeList nodeList = arrMods[i];
                for (int j = 0; j < nodeList.getLength(); j++) {
                    l.add(nodeList.item(j));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return l;

    }

    public Document getDescriptorAsDOM(String modName) throws IOException {
//        print("getModuleDescriptor: " + modName);

        String descPath = null;
        if (modName.endsWith(".jar")) {
            descPath = "META-INF/ejb-jar.xml";
        } else if (modName.endsWith(".rar")) {
            descPath = "META-INF/ra.xml";
        }

//        print("getModuleDescriptor: descPath: " + descPath);

        InputStream ismod = zf.getInputStream(zf.getEntry(modName));
        ZipInputStream zis = new ZipInputStream(ismod);
        ZipEntry zentry = null;
        while ((zentry = zis.getNextEntry()) != null) {
            ///print("descPath " + descPath);
            if (descPath.equals(zentry.getName())) {
                return DOMUtil.parse(zis);
            }
        }
        return null;
    }

    public String getDescriptorAsString(String modName) throws IOException {
//        print("getModuleDescriptor: " + modName);

        String descPath = null;
        if (modName.endsWith(".jar")) {
            descPath = "META-INF/ejb-jar.xml";
        } else if (modName.endsWith(".rar")) {
            descPath = "META-INF/ra.xml";
        }

//        print("getModuleDescriptor: descPath: " + descPath);

        InputStream ismod = zf.getInputStream(zf.getEntry(modName));
        ZipInputStream zis = new ZipInputStream(ismod);
        ZipEntry zentry = null;
        while ((zentry = zis.getNextEntry()) != null) {
            ///print("descPath " + descPath);
            if (descPath.equals(zentry.getName())) {
                
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(zis, "UTF-8"));
                StringBuffer sb = new StringBuffer();
                char[] cbuf = new char[2048];
                int len = 0;
                while ( (len = br.read(cbuf, 0, 2048)) != -1) {
                    sb.append(cbuf, 0, len);
                }
                return sb.toString();
            }
        }
        
        return null;
    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
