/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.glassfish.openesb.addons.logredirector;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Overrides a record's getThrown() method to return null
 * 
 * @author fkieviet
 */
public class NoStackTraceLogRecord extends LogRecord implements java.io.Serializable {
    private static final long serialVersionUID = 2614615547478196947L;
    private LogRecord delegate;

    /**
     * @param delegate record to delegate to
     */
    public NoStackTraceLogRecord(LogRecord delegate) {
        super(delegate.getLevel(), delegate.getMessage());
        this.delegate = delegate;
    }

    /**
     * @see java.util.logging.LogRecord#getLevel()
     */
    @Override
    public Level getLevel() {
        return delegate.getLevel();
    }

    /**
     * @see java.util.logging.LogRecord#getLoggerName()
     */
    @Override
    public String getLoggerName() {
        return delegate.getLoggerName();
    }

    /**
     * @see java.util.logging.LogRecord#getMessage()
     */
    @Override
    public String getMessage() {
        return delegate.getMessage();
    }

    /**
     * @see java.util.logging.LogRecord#getMillis()
     */
    @Override
    public long getMillis() {
        return delegate.getMillis();
    }

    /**
     * @see java.util.logging.LogRecord#getParameters()
     */
    @Override
    public Object[] getParameters() {
        return delegate.getParameters();
    }

    /**
     * @see java.util.logging.LogRecord#getResourceBundle()
     */
    @Override
    public ResourceBundle getResourceBundle() {
        return delegate.getResourceBundle();
    }

    /**
     * @see java.util.logging.LogRecord#getResourceBundleName()
     */
    @Override
    public String getResourceBundleName() {
        return delegate.getResourceBundleName();
    }

    /**
     * @see java.util.logging.LogRecord#getSequenceNumber()
     */
    @Override
    public long getSequenceNumber() {
        return delegate.getSequenceNumber();
    }

    /**
     * @see java.util.logging.LogRecord#getSourceClassName()
     */
    @Override
    public String getSourceClassName() {
        return delegate.getSourceClassName();
    }

    /**
     * @see java.util.logging.LogRecord#getSourceMethodName()
     */
    @Override
    public String getSourceMethodName() {
        return delegate.getSourceMethodName();
    }

    /**
     * @see java.util.logging.LogRecord#getThreadID()
     */
    @Override
    public int getThreadID() {
        return delegate.getThreadID();
    }

    /**
     * RETURN NULL
     * 
     * @see java.util.logging.LogRecord#getThrown()
     */
    @Override
    public Throwable getThrown() {
        return null;
    }
}
