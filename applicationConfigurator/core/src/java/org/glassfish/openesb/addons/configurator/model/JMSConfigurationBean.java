/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSConfigurationBean.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.model;

import java.util.HashMap;
import java.util.Map;



/**
 * JMS Configuration object is JMS configurable bean object.
 * 
 * @author Sun Microsystems
 *
 */
public class JMSConfigurationBean {
	public static final byte GENERAL = 0, JMS_REP_ENVIRONMENT = 1, JMSJCA_CONNECTION_POOL = 2;
	private String mName, mDisplayName, mDescription;
	private Map <String, JMSProperty> mProperties = new HashMap <String, JMSProperty>();	
	private String mConnUrl, mUsername, mPwd, mOptions;

	public String getConnetionUrl() {
		return mConnUrl;
	}

	public void setConnetionUrl(String connUrl) {
		mConnUrl = connUrl;
	}

	public String getUsername() {
		return mUsername;
	}

	public void setUsername(String username) {
		mUsername = username;
	}

	public String getPassword() {
		return mPwd;
	}

	public void setPassword(String pwd) {
		mPwd = pwd;
	}

	public String getOptions() {
		return mOptions;
	}

	public void setOptions(String options) {
		mOptions = options;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getDisplayName() {
		return mDisplayName;
	}

	public void setDisplayName(String displayName) {
		mDisplayName = displayName;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String discription) {
		mDescription = discription;
	}
	
	public JMSProperty createProperty(String name) {
		return new JMSProperty(this, name);
	}
	
	public void addProperty(JMSProperty prop) {
		mProperties.put(prop.getName(), prop);
	}
	
	public Map<String, JMSProperty> getProperties() {
		return mProperties;
	}
	
}
