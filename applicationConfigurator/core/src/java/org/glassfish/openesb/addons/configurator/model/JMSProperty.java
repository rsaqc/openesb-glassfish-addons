/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSProperty.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.configurator.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

//Referenced classes of package org.glassfish.openesb.addons.configurator.model:
//JMSConfigurationBean

public class JMSProperty
{

	public JMSProperty(JMSConfigurationBean parent, String name)
	{
		isCollection = false;
		isChoice = false;
		isChoiceEditable = false;
		isReadable = true;
		isWritable = true;
		isEnabled = true;
		isReference = false;
		isLDAPDisabled = false;
		isFinalFlag = false;
		mParent = parent;
		setName(name);
		value = new ArrayList();
		def = new ArrayList();
		choiceCollection = new ArrayList();
	}

	public ArrayList getRawValue()
	{
		return value;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String n)
	{
		name = n;
	}

	public Object getDefault()
	{
		if(isCollection)
			if(def.size() == 0)
				return null;
			else
				return def;
		if(isChoiceEditable)
			if(def.size() == 0)
				return null;
			else
				return def;
		if(def.size() == 0)
			return null;
		else
			return def.get(0);
	}

	public void setDefault(Object d)
	{
		if(isCollection)
			def.add(d);
		else
			if(def.size() > 0)
				def.set(0, d);
			else
				def.add(d);
	}

	public String getValue()
	{
		return mValue;
	}

	public void setValue(String obj)
	{
		mValue = obj;
	}

	public void setContextValidator(Object obj)
	{
		validator = obj;
	}

	public String getDescription()
	{
		return description;
	}

	public String getUserComment()
	{
		return userComment;
	}

	public void setDescription(String s)
	{
		description = s;
	}

	public void setUserComment(String c)
	{
		userComment = c;
	}

	protected void setValueImpl(Object o)
	{
		if(isReference())
		{
			value.clear();
			value.add(o);
		} else
			if(isCollection)
				value.add(o);
			else
				if(isChoiceEditable)
				{
					addChoice(o);
					if(value.size() > 0)
						value.set(0, o);
					else
						value.add(o);
				} else
					if(value.size() > 0)
						value.set(0, o);
					else
						value.add(o);
	}

	public void add(Object obj)
	{
		value.add(obj);
	}

	public Object get(int idx)
	{
		return value.get(idx);
	}

	public int getMaxSize()
	{
		return maxSize;
	}

	public int getMinSize()
	{
		return minSize;
	}

	public Object remove(Object obj)
	{
		int idx = value.indexOf(obj);
		if(idx == -1)
			return null;
		else
			return value.remove(idx);
	}

	public Object removeElementAt(int idx)
	{
		return value.remove(idx);
	}

	public void reset()
	{
		value.clear();
	}

	public void set(int idx, Object obj)
	{
		value.set(idx, obj);
	}

	public void setMaxSize(int s)
	{
		maxSize = s;
	}

	public void setMinSize(int s)
	{
		minSize = s;
	}

	public void setIsCollection(boolean isCollection)
	{
		this.isCollection = isCollection;
	}

	public boolean isReadable()
	{
		return isReadable;
	}

	public boolean isWritable()
	{
		return isWritable;
	}

	public void setIsReadable(boolean isReadable)
	{
		this.isReadable = isReadable;
	}

	public void setIsWritable(boolean isWritable)
	{
		this.isWritable = isWritable;
	}

	public boolean isCollection()
	{
		return isCollection;
	}

	public String getDisplayName()
	{
		if(displayName == null)
			return getName();
		else
			return displayName;
	}

	public void setDisplayName(String n)
	{
		displayName = n;
	}

	public void addChoice(Object p)
	{
		if(!choiceCollection.contains(p))
			choiceCollection.add(p);
	}

	public Collection getChoiceCollection()
	{
		return choiceCollection;
	}

	public boolean isChoice()
	{
		return isChoice;
	}

	public Object removeChoice(Object p)
	{
		int idx = choiceCollection.indexOf(p);
		if(idx == -1)
			return null;
		else
			return choiceCollection.remove(idx);
	}

	public void setIsChoice(boolean v)
	{
		isChoice = v;
	}

	public Object removeChoiceElementAt(int idx)
	{
		return choiceCollection.remove(idx);
	}

	public void addDefault(Object d)
	{
		setDefault(d);
	}

	public Object removeDefault(Object d)
	{
		int idx = def.indexOf(d);
		if(idx == -1)
			return null;
		else
			return def.remove(idx);
	}

	public Object removeDefaultAt(int idx)
	{
		return def.remove(idx);
	}

	public boolean isEnabled()
	{
		return isEnabled;
	}

	public void setIsEnabled(boolean v)
	{
		isEnabled = v;
	}

	public void setParent(JMSConfigurationBean s)
	{
		mParent = s;
	}

	public boolean isReference()
	{
		return isReference;
	}

	public void setIsReference(boolean b)
	{
		isReference = b;
	}

	public boolean isChoiceEditable()
	{
		return isChoiceEditable;
	}

	public void setIsChoiceEditable(boolean v)
	{
		isChoiceEditable = v;
	}

	public boolean isFinal()
	{
		return isFinalFlag;
	}

	public void setIsFinal(boolean b)
	{
		isFinalFlag = b;
	}

	public Object clone()
	throws CloneNotSupportedException
	{
		return super.clone();
	}

	protected ArrayList value;
	private ArrayList choiceCollection;
	private String name;
	private String displayName;
	private Object validator;
	private String userComment;
	private String description;
	private int minSize;
	private int maxSize;
	private boolean isCollection;
	private boolean isChoice;
	private boolean isChoiceEditable;
	private boolean isReadable;
	private boolean isWritable;
	private ArrayList def;
	private boolean isEnabled;
	private boolean isReference;
	private boolean isLDAPDisabled;
	private boolean isFinalFlag;
	private transient Logger mLog;
	private JMSConfigurationBean mParent;
	private String mValue;
}

