/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DataBean.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.addons.configurator.beans;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.appserv.management.config.ConnectorConnectionPoolConfig;
import com.sun.appserv.management.config.CustomResourceConfig;
import org.glassfish.openesb.addons.configurator.model.ConfigurableBean;
import org.glassfish.openesb.addons.configurator.util.CapsConstants;
import com.sun.data.provider.TableDataProvider;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.enterprise.tools.admingui.util.AMXUtil;

public class DataBean
{
	private final static String CONFIG_PROP_NAME = "Configuration";
    /**
     * cached data for table row group
     */
    private TableDataProvider mCachedTableData = null;
    List<ConfigurableBean> mConfigurable = new ArrayList<ConfigurableBean>();
    public TableDataProvider getSharedTableData()
    {

       TableDataProvider result = mCachedTableData;

       return result;
    }
    
    public List<ConfigurableBean> getCapsTreeData() {
    	List<ConfigurableBean> result = mConfigurable;
    	return result;
    }
    
    public void setConnectorConnectionPoolTableData()
    {
    	Set keys = AMXUtil.getDomainConfig().getConnectorConnectionPoolConfigMap().keySet();
    	List names = new ArrayList(keys);
    	mConfigurable.clear();
    	for (int i = 0; i < names.size(); i++) {
    		
    		ConnectorConnectionPoolConfig pool = AMXUtil.getDomainConfig().getConnectorConnectionPoolConfigMap().get((String)names.get(i));
    		
    		String adapterName = ((ConnectorConnectionPoolConfig) pool).getResourceAdapterName();
    		if (CapsConstants.JMS_ADAPTER_NAME.equals(adapterName)) {
    			ConfigurableBean bean = new ConfigurableBean();
    			bean.setName(names.get(i).toString());
    			bean.setEncodedData("");       		
        		mConfigurable.add(bean);
    		} else {
				Map<String, String> props = pool.getProperties();
				if (props.containsKey(CONFIG_PROP_NAME)) {
					ConfigurableBean bean = new ConfigurableBean();
					bean.setName(names.get(i).toString());
					bean.setEncodedData(props.get(CONFIG_PROP_NAME));
					mConfigurable.add(bean);
				}
    		}
    	}
        TableDataProvider result =
        new ObjectListDataProvider (mConfigurable);
        mCachedTableData = result;
     }
    
  

}